<!-- MySchedule - Carlos Ferreira -->
<?php
session_start();
if (isset($_SESSION['userid'])) {

    require '../database.php';
    $session_id = $_SESSION['userid'];

    if (isset($_POST['assocbtn'])) {

        $id_nota_assoc = $_POST['id_nota'];

        $sql_delete_all_reg_from_assoc = "DELETE FROM assoclabels WHERE idNota = ? AND idUtilizador = ?";
        $stmt_delete_all_reg_from_assoc = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt_delete_all_reg_from_assoc, $sql_delete_all_reg_from_assoc)) {
            header("Location: ../../html/actions.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt_delete_all_reg_from_assoc, "ss", $id_nota_assoc, $session_id);
            mysqli_stmt_execute($stmt_delete_all_reg_from_assoc);
        }

        $sql = "SELECT * FROM labels WHERE idUtilizador = ?";
        $stmt = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../../../../../html/actions.php?error=cantpreparestmt");
        } else {
            mysqli_stmt_bind_param($stmt, "s", $session_id);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);

            $sql_delete_if_not_selected = "DELETE FROM assoclabels WHERE idNota = ? AND idUtilizador = ?";
            $stmt_delete_if_not_selected = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt_delete_if_not_selected, $sql_delete_if_not_selected)) {
                header("Location: ../../html/actions.php?error=cantpreparestmt");
            } else {
                mysqli_stmt_bind_param($stmt_delete_if_not_selected, "ss", $id_nota_assoc, $session_id);
                mysqli_stmt_execute($stmt_delete_if_not_selected);
            }

            while ($row = $result->fetch_array()) {
                $id_checkbox = $row['idLabel'];
                $tmp = "etiqueta_" . $id_checkbox;

                if (isset($_POST[$tmp])) {
                    $id_label_assoc = $_POST[$tmp];

                    $sql_assoc_notes_labels = "INSERT INTO assoclabels (idLabel, idNota, idUtilizador) VALUES (?,?,?)";
                    $stmt_assoc_notes_labels = mysqli_stmt_init($connection);

                    if (!mysqli_stmt_prepare($stmt_assoc_notes_labels, $sql_assoc_notes_labels)) {
                        header("Location: ../.. /../../../../../html/actions.php?error=cantpreparestmt");
                    } else {
                        echo $sql_assoc_notes_labels . "<br>";
                        echo $id_nota_assoc . "<br>";
                        echo $id_label_assoc . "<br>";
                        mysqli_stmt_bind_param($stmt_assoc_notes_labels, "sss", $id_label_assoc, $id_nota_assoc, $session_id);
                        mysqli_stmt_execute($stmt_assoc_notes_labels);

                        header("Location: http://localhost/myschedule/html/actions.php");
                    }
                }else{
                    header("Location: http://localhost/myschedule/html/actions.php");
                }
            }
        }
    } else {
        header("Location: /../../../../html/actions.php?error=assocbtnnotclicked");
    }
} else {
    header("Location: /../../../../html/actions.php?error=nosession");
}
