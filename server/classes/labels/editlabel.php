<!-- MySchedule - Carlos Ferreira -->
<?php
session_start();

if (isset($_SESSION['userid'])) {

    require '../database.php';

    if (isset($_POST['editlabelbtn'])) {
        $session_id = $_SESSION["userid"];
        $newname_label = $_POST['newnamelabel'];
        $idlabel = $_POST['id_label'];
        $sql = "UPDATE labels SET nomeLabel = ? WHERE idLabel = ?";
        $stmt = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../../../../../html/actions.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "si", $newname_label,$idlabel);
            mysqli_stmt_execute($stmt);
            header("Location: http://localhost/myschedule/html/actions.php");
            exit();
        }
    }
} else {
    header("Location: ../../../../../html/actions.php");
}
?>