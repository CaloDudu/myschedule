<!-- MySchedule - Carlos Ferreira -->
<?php
session_start();

if (isset($_SESSION['userid'])) {

    require '../database.php';

    if (isset($_POST['insertlabelbtn'])) {
        $session_id = $_SESSION["userid"];
        $nome_label = $_POST['nomelabel'];
        if (empty($nome_label)) {
            header("Location: http://localhost/myschedule/html/actions.php");
            exit();
        }
        else{
            $sql = "INSERT INTO labels(nomeLabel,idUtilizador) VALUES (?,?)";
            $stmt = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt, $sql)) {
                header("Location: ../../../../../html/actions.php?error=cantpreparestmt");
                exit();
            }
            else {
                mysqli_stmt_bind_param($stmt, "ss", $nome_label, $session_id);
                mysqli_stmt_execute($stmt);

                header("Location: http://localhost/myschedule/html/actions.php");
                exit();
            }
        }
    }
} else {
    header("Location: ../../../../../html/actions.php");
}
?>