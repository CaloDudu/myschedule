<!-- MySchedule - Carlos Ferreira -->
<?php
session_start();
require '../database.php';
if (isset($_SESSION['userid'])) {
    if (isset($_GET['id'])) {
        $id_label = $_GET['id'];
        $sql = "DELETE FROM labels WHERE idLabel = ?";
        $stmt = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../../html/actions.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "i", $id_label);
            mysqli_stmt_execute($stmt);

            header("Location: http://localhost/myschedule/html/actions.php");
            exit();
        }
    } else {
        header("Location: ../../../../../html/actions.php?error=nogetvalue");
    }
} else {
    header("Location: ../../../../../html/actions.php?error=nosession");
}
