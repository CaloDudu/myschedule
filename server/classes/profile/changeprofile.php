<?php
require '../database.php';
session_start();
if (isset($_SESSION['userid'])) {
     if (isset($_POST['changeprofile'])) {
          $username = $_POST['nome'];
          $email = $_POST['icon_telephone'];
          $userid = $_SESSION['userid'];

          //Receber email original de determinado id
          $sql_receive_email = "SELECT * FROM users WHERE idUtilizador = ?";
          $stmt_receive_email = mysqli_stmt_init($connection);

          if (!mysqli_stmt_prepare($stmt_receive_email, $sql_receive_email)) {
               header("Location: ../../../html/todo.php?error=cantpreparestmtusername");
               exit();
          } else {
               mysqli_stmt_bind_param($stmt_receive_email, "s", $userid);
               mysqli_stmt_execute($stmt_receive_email);

               $result_receive_email = mysqli_stmt_get_result($stmt_receive_email);
               if ($row_receive_email = mysqli_fetch_assoc($result_receive_email)) {
                    $emailoriginal = $row_receive_email['emailUtilizador'];
               }
          }
          //Verificação -> Campos Vazios
          if ($username != "") {
               //Verificação -> Existência de utilizador inserido
               $sql_verify_existing_fields_username = "SELECT * FROM users WHERE nomeUtilizador = ?";
               $stmt_verify_existing_fields_username = mysqli_stmt_init($connection);

               if (!mysqli_stmt_prepare($stmt_verify_existing_fields_username, $sql_verify_existing_fields_username)) {
                    header("Location: ../../../html/todo.php?error=cantpreparestmtusername");
                    exit();
               } else {
                    mysqli_stmt_bind_param($stmt_verify_existing_fields_username, "s", $username);
                    mysqli_stmt_execute($stmt_verify_existing_fields_username);

                    $result_data_existing_username = mysqli_stmt_get_result($stmt_verify_existing_fields_username);
                    $countrows_username = $result_data_existing_username->num_rows;

                    if ($countrows_username == 0) {
                         //Update username
                         $sql_update_username = "UPDATE users SET nomeUtilizador = ? WHERE idUtilizador = ?";
                         $stmt_update_username = mysqli_stmt_init($connection);

                         if (!mysqli_stmt_prepare($stmt_update_username, $sql_update_username)) {
                              header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmtusername");
                              exit();
                         } else {
                              mysqli_stmt_bind_param($stmt_update_username, "ss", $username, $userid);
                              mysqli_stmt_execute($stmt_update_username);
                         }
                    } else {
                         header("Location: ../../../html/todo.php?error=existingusername");
                    }
               }
          }

          if ($email != "" && filter_var($email, FILTER_VALIDATE_EMAIL)) {
               //Verificação -> Existência de email inserido
               $sql_verify_existing_fields_email = "SELECT * FROM users WHERE emailUtilizador = ?";
               $stmt_verify_existing_fields_email = mysqli_stmt_init($connection);

               if (!mysqli_stmt_prepare($stmt_verify_existing_fields_email, $sql_verify_existing_fields_email)) {
                    header("Location: ../../../html/todo.php?error=cantpreparestmtemail");
                    exit();
               } else {
                    mysqli_stmt_bind_param($stmt_verify_existing_fields_email, "s", $email);
                    mysqli_stmt_execute($stmt_verify_existing_fields_email);

                    $result_data_existing_email = mysqli_stmt_get_result($stmt_verify_existing_fields_email);
                    $countrows_email = $result_data_existing_email->num_rows;

                    if ($countrows_email == 0) {
                         //Update Email
                         $sql_update_email = "UPDATE users SET emailUtilizador = ? WHERE idUtilizador = ?";
                         $stmt_update_email = mysqli_stmt_init($connection);

                         if (!mysqli_stmt_prepare($stmt_update_email, $sql_update_email)) {
                              header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmtemail");
                              exit();
                         } else {
                              mysqli_stmt_bind_param($stmt_update_email, "ss", $email, $userid);
                              mysqli_stmt_execute($stmt_update_email);


                              $sql = "UPDATE sharestasks SET emailDestino = ? WHERE emailDestino = ?";
                              $stmt = mysqli_stmt_init($connection);

                              if (!mysqli_stmt_prepare($stmt, $sql)) {
                                   header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmtupdateemailshares");
                                   exit();
                              } else {
                                   mysqli_stmt_bind_param($stmt, "ss", $email, $emailoriginal);
                                   mysqli_stmt_execute($stmt);
                              }

                              $sql1 = "UPDATE shares SET emailDestino = ? WHERE emailDestino = ?";
                              $stmt1 = mysqli_stmt_init($connection);

                              if (!mysqli_stmt_prepare($stmt1, $sql1)) {
                                   header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmtupdateemailshares");
                                   exit();
                              } else {
                                   mysqli_stmt_bind_param($stmt1, "ss", $email, $emailoriginal);
                                   mysqli_stmt_execute($stmt1);
                              }

                              header("Location: http://localhost/myschedule/html/todo.php?modal=changedprofile");
                         }
                    } else {
                         header("Location: ../../../html/todo.php?error=existingemail");
                    }
               }
          } else {
               header("Location: ../../../html/todo.php");
          }
     } else {
          header("Location: ../../../html/todo.php");
     }
} else {
     header("Location: ../../../html/index.php?error=nosession");
}
