<?php
session_start();
require '../database.php';
if (isset($_SESSION['userid'])) {
    $session_id = $_SESSION['userid'];
    if (isset($_GET['id'])) {
        $idtask = $_GET['id'];
        $sql = "UPDATE tasks SET status = 6 WHERE idTask = ?";
        $stmt = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../../html/todo.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "s", $idtask);
            mysqli_stmt_execute($stmt);

            $sql_delete_reg_from_shared_tables = "DELETE FROM sharestasks WHERE idTask = ?";
            $stmt_delete_reg_from_shared_tables = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt_delete_reg_from_shared_tables, $sql_delete_reg_from_shared_tables)) {
                header("Location: ../../html/todo.php?error=cantpreparestmt");
                exit();
            } else {
                mysqli_stmt_bind_param($stmt_delete_reg_from_shared_tables, "i", $idtask);
                mysqli_stmt_execute($stmt_delete_reg_from_shared_tables);

                header("Location: http://localhost/myschedule/html/todo.php");
            }
        }
    }
} else {
    header("Location: ../../../../../html/index.php?nosession");
}
