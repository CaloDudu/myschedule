<?php
session_start();
require '../database.php';
if (isset($_SESSION['userid'])) {
    $session_id = $_SESSION['userid'];
    if (isset($_GET['id'])) {
        $idtask = $_GET['id'];
        $sql = "DELETE FROM tasks WHERE status = 6 AND idTask = ? AND idUtilizador = ?";
        $stmt = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../../html/todo.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "ss", $idtask, $session_id);
            mysqli_stmt_execute($stmt);

            header("Location: http://localhost/myschedule/html/todo.php");
            exit();
        }
    }
} else {
    header("Location: ../../../../../html/index.php?nosession");
}
