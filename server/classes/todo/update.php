<?php
session_start();
require '../database.php';

if (isset($_SESSION['userid'])) {
    if (isset($_POST['inserTaskButton'])) {
        $name_task = $_POST['name_task_write'];
        $priority = $_POST['selectPriorityUpdate'];
        $datetask = $_POST['dataTaskEditar'];
        $idtask = $_POST['id'];
        $iduser = $_SESSION['userid'];

        $datetask_exploded = explode(" ", $datetask);
        $mestask = $datetask_exploded[0];
        $diatask = $datetask_exploded[1];
        $anotask = $datetask_exploded[2];
        $diatask_exploded = explode(",", $diatask);

        $mestask_toint = 0;
        if ($mestask == 'Jan') {
            $mestask_toint = 1;
        } else if ($mestask == 'Fev') {
            $mestask_toint = 2;
        } else if ($mestask == 'Mar') {
            $mestask_toint = 3;
        } else if ($mestask == 'Abr') {
            $mestask_toint = 4;
        } else if ($mestask == 'Mai') {
            $mestask_toint = 5;
        } else if ($mestask == 'Jun') {
            $mestask_toint = 6;
        } else if ($mestask == 'Jul') {
            $mestask_toint = 7;
        } else if ($mestask == 'Ago') {
            $mestask_toint = 8;
        } else if ($mestask == 'Set') {
            $mestask_toint = 9;
        } else if ($mestask == 'Out') {
            $mestask_toint = 10;
        } else if ($mestask == 'Nov') {
            $mestask_toint = 11;
        } else if ($mestask == 'Dez') {
            $mestask_toint = 12;
        }

        $data = $anotask . "-" . $mestask_toint . "-" . $diatask_exploded[0];

        if ($data == 0) {
            $sql_update_data = "UPDATE tasks SET nomeTask = ?, importanciaTask = ? WHERE idTask = ? AND idUtilizador = ?";
            $stmt_update_data = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt_update_data, $sql_update_data)) {
                header("Location: /../../../../html/todo.php");
            } else {
                mysqli_stmt_bind_param($stmt_update_data, "ssss", $name_task, $priority, $idtask, $iduser);
                mysqli_stmt_execute($stmt_update_data);

                header("Location: http://localhost/myschedule/html/todo.php");
            }
        }else{
            $sql_update_data = "UPDATE tasks SET nomeTask = ?, dataLimiteTask = ?, importanciaTask = ? WHERE idTask = ? AND idUtilizador = ?";
            $stmt_update_data = mysqli_stmt_init($connection);
    
            if (!mysqli_stmt_prepare($stmt_update_data, $sql_update_data)) {
                header("Location: /../../../../html/todo.php");
            } else {
                mysqli_stmt_bind_param($stmt_update_data, "sssss", $name_task, $data, $priority, $idtask, $iduser);
                mysqli_stmt_execute($stmt_update_data);
    
                header("Location: http://localhost/myschedule/html/todo.php");
            }
        }
    } else {
        echo "button not clicked";
    }
} else {
    echo "no session";
}
