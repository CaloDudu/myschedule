<?php
session_start();
require '../database.php';

if ($_SESSION['userid']) {
    if ($_GET['id']) {
        $idtask = $_GET['id'];

        $sql = "UPDATE tasks SET status = 0 WHERE idTask = ?";
        $stmt = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../../html/todo.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "i", $idtask);
            mysqli_stmt_execute($stmt);

            header("Location: http://localhost/myschedule/html/todo.php");
            exit();
        }
    } else { }
} else {
    header("Location: ../../../../../html/index.php?error=nosession");
}
