<!-- MySchedule - Carlos Ferreira -->
<?php
    session_start();

    if(isset($_POST["refuse_task_btn"])){
        //Receber dados para query
        //Set Resultado = 0 -> Partilha Recusada
        $id_task = $_POST['id_nota'];
        $id_origin_input = $_POST['id_origin_input'];
        $email_session = $_POST['email_session'];

        require ('../../database.php');
            
        $session_id = $_SESSION["userid"];

        $stmt = mysqli_stmt_init($connection);
        $sql = "UPDATE sharestasks SET resultado_trans = 0 WHERE idTask = ? AND idOrigem = ? AND emailDestino = ?";

        if(!mysqli_stmt_prepare($stmt, $sql)){
            echo "Erro";
        }
        else{
            mysqli_stmt_bind_param($stmt, "iis", $id_task, $id_origin_input, $email_session);
            mysqli_stmt_execute($stmt);

            header("Location: ../../../../../../myschedule/html/todo.php");
        }  
    }
?>