      <!-- MySchedule - Carlos Ferreira -->
      <?php
      session_start();

      $storage = $_POST["storage"];
      $emails = explode(" ", $storage);
      $id_task = $_POST["id_share_task"];
      $idtask_temp = $id_task;

      $resultado_inicial = 0;

      if (isset($_SESSION['userid'])) {
         require('../../database.php');

         $session_id = $_SESSION["userid"];

         $emailsTotal = $emails;

         $value = count($emailsTotal);

         //Verificação de email -> Notificação a si próprio
         //Verificação de email -> Repetição de Notificações

         for ($i = 0; $i < $value; $i++) {
            //Verificação de dados
            if (!filter_var($emailsTotal[$i], FILTER_VALIDATE_EMAIL) || $emailsTotal[$i] == "") {
               unset($emailsTotal[$i]);
            } else {
               $stmt = mysqli_stmt_init($connection);
               $sql_get_email = "SELECT emailUtilizador FROM users WHERE idUtilizador = ?";
               $sql = "INSERT INTO sharestasks(idTask, idOrigem, emailDestino, resultado_trans) VALUES (?,?,?,?)";
               $sql_update = "UPDATE sharestasks SET resultado = 0 WHERE idTask = ? AND idOrigem = ? AND emailDestino = ?";

               if (!mysqli_stmt_prepare($stmt, $sql_get_email)) {
                  echo "erro";
               } else {
                  mysqli_stmt_bind_param($stmt, "i", $session_id);
                  mysqli_stmt_execute($stmt);

                  $result = mysqli_stmt_get_result($stmt);

                  if ($row = mysqli_fetch_assoc($result)) {
                     $email_utilizador = $row['emailUtilizador'];
                  }
               }

               if ($emailsTotal[$i] == $email_utilizador) {
                  unset($emailsTotal[$i]);
               } else {
                  $sql_verify_duplicated_reg = "SELECT * FROM sharestasks WHERE idTask = ? AND idOrigem = ? AND emailDestino = ?";
                  if (!mysqli_stmt_prepare($stmt, $sql_verify_duplicated_reg)) {
                     echo "erro_duplicated";
                  } else {
                     mysqli_stmt_bind_param($stmt, "sss", $idtask_temp, $session_id, $emailsTotal[$i]);
                     mysqli_stmt_execute($stmt);

                     $result_values = mysqli_stmt_get_result($stmt);

                     //PHP 7.0 - Num_Rows
                     $count_rows = $result_values->num_rows;
                  }
               }

               if ($count_rows == 0) {
                  //Inserir registo
                  if (!mysqli_stmt_prepare($stmt, $sql)) {
                     echo "erro_count_num_rows";
                  } else {
                     mysqli_stmt_bind_param($stmt, "ssss", $idtask_temp, $session_id, $emailsTotal[$i], $resultado_inicial);
                     mysqli_stmt_execute($stmt);

                     header("Location: http://localhost/myschedule/html/todo.php?modal=sent_task_sucess");
                     exit();
                  }
               } else {
                  //Update
                  if (!mysqli_stmt_prepare($stmt, $sql_update)) {
                     echo "erro";
                  } else {
                     mysqli_stmt_bind_param($stmt, "sssi", $idtask_temp, $session_id, $emailsTotal[$i]);
                     mysqli_stmt_execute($stmt);

                     header("Location: http://localhost/myschedule/html/todo.php");
                  }
               }
            }
         }
      } else {
         header("Location ../../../../../../html/index.php?error=nologin");
      }
      ?>