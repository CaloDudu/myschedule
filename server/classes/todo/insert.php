<?php
    session_start();
    if(isset($_SESSION['userid'])){
        require '../database.php';
        $id_user = $_SESSION['userid'];
        if(isset($_POST['insertTasksButton'])){
            $nametask = $_POST['nameEtiquetaValue'];
            $priority = $_POST['selectPriority'];
            $datetask = $_POST['dataTask'];

            $datetask_exploded = explode(" ", $datetask);
            $mestask = $datetask_exploded[0];
            $diatask = $datetask_exploded[1];
            $anotask = $datetask_exploded[2];            
            $diatask_exploded = explode(",",$diatask);

            $mestask_toint = 0;
            if($mestask == 'Jan'){
                $mestask_toint = 1;
            }else if($mestask == 'Fev'){
                $mestask_toint = 2;
            }else if($mestask == 'Mar'){
                $mestask_toint = 3;
            }else if($mestask == 'Abr'){
                $mestask_toint = 4;
            }else if($mestask == 'Mai'){
                $mestask_toint = 5;
            }else if($mestask == 'Jun'){
                $mestask_toint = 6;
            }else if($mestask == 'Jul'){
                $mestask_toint = 7;
            }else if($mestask == 'Ago'){
                $mestask_toint = 8;
            }else if($mestask == 'Set'){
                $mestask_toint = 9;
            }else if($mestask == 'Out'){
                $mestask_toint = 10;
            }else if($mestask == 'Nov'){
                $mestask_toint = 11;
            }else if($mestask == 'Dez'){
                $mestask_toint = 12;
            }
            
            //yyyy-mm-dd

            $compile_package_date = $anotask."-".$mestask_toint."-".$diatask_exploded[0];
            echo $compile_package_date;

            $sql = "INSERT INTO tasks(nomeTask, dataLimiteTask, importanciaTask, idUtilizador) VALUES (?,?,?,?)";
            $stmt = mysqli_stmt_init($connection);

            if(!mysqli_stmt_prepare($stmt, $sql)){  
                header("Location: ../../../html/todo.php?error=cantpreparestmt");
                exit();
            }
            else{
                mysqli_stmt_bind_param($stmt, "ssss", $nametask, $compile_package_date, $priority, $id_user);
                mysqli_stmt_execute($stmt);

                header("Location: http://localhost/myschedule/html/todo.php"); 
            }
        }
    }
?>