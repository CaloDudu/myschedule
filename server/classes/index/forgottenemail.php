     <?php
     use PHPMailer\PHPMailer\PHPMailer;
     use PHPMailer\PHPMailer\Exception;

     require '../phpmailer/src/Exception.php';
     require '../phpmailer/src/PHPMailer.php';
     require '../phpmailer/src/SMTP.php';
     require '../database.php';
     session_start();
     if (isset($_POST['get-email-submit'])) {
          $email = $_POST['getemail'];
          echo "entrei";

          $sql_get_data_from_email = "SELECT * FROM users WHERE emailUtilizador = ?";
          $stmt_get_data_from_email = mysqli_stmt_init($connection);

          if (!mysqli_stmt_prepare($stmt_get_data_from_email, $sql_get_data_from_email)) {
               header("Location: ../../../../../html/index.php?error=cantpreparestmt");
               exit();
          } else {
               mysqli_stmt_bind_param($stmt_get_data_from_email, "s", $email);
               mysqli_stmt_execute($stmt_get_data_from_email);

               $result = mysqli_stmt_get_result($stmt_get_data_from_email);

               if ($row = mysqli_fetch_assoc($result)) {
                    $username = $row['nomeUtilizador'];

                    //Enviar Email
                    $destino = $email;
                    $subject = "Pedido de recuperacao de conta";
                    $url = "http://localhost/myschedule/html/index.php";

                    $mensagem = '<p>Recebemos um pedido de recuperação de conta.</p><br>A sua conta é: ' . $username;
                    $mensagem .= '<p><b>Para entrar na aplicação myschedule clique em </b><a href="' . $url . '">' . $url . '</a></p>';

                    $mail = new PHPMailer();
                    $mail->isSMTP();
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = 'ssl';
                    $mail->Host = 'tls://smtp.gmail.com:587';
                    $mail->Port = '465';
                    $mail->isHTML();
                    $mail->Username = 'myschedulebot@gmail.com';
                    $mail->Password = 'myschedule123';
                    $mail->SetFrom('myschedulebot@gmail.com');
                    $mail->Subject = $subject;
                    $mail->Body = $mensagem;
                    $mail->AddAddress($destino);

                    if (!$mail->send()) {
                         echo 'A mensagem não foi enviada.';
                         echo 'Erro: ' . $mail->ErrorInfo;
                    } else {
                         header("Location: http://localhost/MySchedule/html/index.php?modal=emailsent");
                    }
               } else {
                    //Verificação: Não existe email
                    header("Location: http://localhost/MySchedule/html/index.php?error=emaildoesntexit");
               }
          }
     }
