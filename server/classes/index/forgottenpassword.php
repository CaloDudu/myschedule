<!-- MySchedule - Carlos Ferreira-->
<?php 
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require '../phpmailer/src/Exception.php';
    require '../phpmailer/src/PHPMailer.php';
    require '../phpmailer/src/SMTP.php';

    require ('../database.php');

    if(isset($_POST['reset-password-submit'])){

        $selector = bin2hex(random_bytes(8));
        $token = random_bytes(32);
        $userEmail = $_POST["email-reset"];

        $url = "http://localhost/MySchedule/html/index.php?selector=" . $selector . "&validator=" . bin2hex($token) . "&emailuser=" . $userEmail . "&modal=reset-password-modal";

        $expires = date("U") + 1800;

        require '../database.php';

        $sql_delete = "DELETE FROM tokens WHERE pwdResetEmail=?";

        $stmt_delete = mysqli_stmt_init($connection);

        if(!mysqli_stmt_prepare($stmt_delete, $sql_delete)){
            header("Location: ../../../html/index.php?error=sqlerror");
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt_delete, "s", $userEmail);
            mysqli_stmt_execute($stmt_delete);

            $sql_select = "SELECT * FROM users WHERE emailUtilizador = ?";
            $stmt_select = mysqli_stmt_init($connection);

            if(!mysqli_stmt_prepare($stmt_select, $sql_select)){
                header("Location: ../../../html/index.php?error=sqlerror");
                exit();
            }
            else{
                mysqli_stmt_bind_param($stmt_select, "s", $userEmail);
                mysqli_stmt_execute($stmt_select);

                $result_select = mysqli_stmt_get_result($stmt_select);

                if(!$row_select = mysqli_fetch_assoc($result_select)){
                    header("Location: ../../../html/index.php?error=badrequest&modal=reset-modal");
                    exit();
                }
                else{
                    $sql_insert = "INSERT INTO tokens (pwdResetEmail,pwdResetSelector, pwdResetToken, pwdResetExpires) VALUES (?,?,?,?);";

                    $stmt_insert = mysqli_stmt_init($connection);

                    if(!mysqli_stmt_prepare($stmt_insert, $sql_insert)){
                        header("Location: ../../../html/index.php?error=sqlerror");
                        exit();
                    }
                    else{
                        $hashedToken = password_hash($token, PASSWORD_DEFAULT);

                        mysqli_stmt_bind_param($stmt_insert, "ssss", $userEmail, $selector, $hashedToken, $expires);
                        mysqli_stmt_execute($stmt_insert);
                    }

                    $destino = $userEmail;
                    $subject = "Renova a tua password";

                    $mensagem = '<p>Recebemos um pedido de renovação de password. Para renovar a sua password clique no link abaixo.</p><br>';
                    $mensagem .= '<p><a href="'. $url . '">' . $url . '</a></p>';


                    $mail = new PHPMailer();
                    $mail->isSMTP();
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = 'ssl';
                    $mail->Host = 'tls://smtp.gmail.com:587';
                    $mail->Port = '465';
                    $mail->isHTML();
                    $mail->Username = 'myschedulebot@gmail.com';
                    $mail->Password = 'myschedule123';
                    $mail->SetFrom('myschedulebot@gmail.com');
                    $mail->Subject = $subject;
                    $mail->Body = $mensagem;
                    $mail->AddAddress($destino);

                    if(!$mail->send()) {
                        echo 'A mensagem não foi enviada.';
                        echo 'Erro: ' . $mail->ErrorInfo;
                    } else {
                        header("Location: http://localhost/MySchedule/html/index.php?modal=emailsent");
                    }
                }
            }
        }
    }
    else{
        header("Location: http://localhost/MySchedule/html/index.php");
    }
?>


