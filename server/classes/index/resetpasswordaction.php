<!-- MySchedule - Carlos Ferreira-->
<?php
    require '../database.php';

    if(isset($_POST['reset-button-submit'])){
        $selector = $_POST["selector"];
        $validator = $_POST["validator"];
        $pwd = $_POST["password"];
        $passwordRepeat = $_POST["password-repeat"];

        if(empty($pwd) || empty($passwordRepeat)){
            header("Location: ../../../html/index.php?error=emptyfields&modal=reset-password-modal");
            exit();
        }
        else if($pwd != $passwordRepeat){
            header("Location: ../../../html/index.php?error=pwddif&selector=" . $selector . "&validator=" . bin2hex($token) . "&emailuser=" . $userEmail . "&modal=reset-password-modal");
            exit();
        }
        else{
            $data = date("U");

            //require '../database.php';
    
            //Verificar se token expirou e existe
            $sql = "SELECT * FROM tokens WHERE pwdResetSelector = ? AND pwdResetExpires >= ?";  
            
            $stmt = mysqli_stmt_init($connection);
    
            if(!mysqli_stmt_prepare($stmt, $sql)){
                header("Location: ../../../html/index.php?error=sqlerror");
                exit();
            }
            else{
                mysqli_stmt_bind_param($stmt, "ss", $selector, $data);
                mysqli_stmt_execute($stmt);
    
                $result = mysqli_stmt_get_result($stmt);

                if(!$row = mysqli_fetch_assoc($result)){
                    header("Location: ../../../html/index.php?error=badrequest1");
                    exit();
                }
                else{
                    //Descodificar o validator
                    $tokenBin = hex2bin($validator);
                    //Verificar se o validator é igual ao token da bd
                    $tokenCheck = password_verify($tokenBin, $row["pwdResetToken"]);
    
                    if ($tokenCheck == false) {
                        header("Location: ../../../html/index.php?error=badrequest");
                        exit();
                    }
                    else if($tokenCheck == true){
    
                        $tokenEmail = $row['pwdResetEmail'];
                        $sql = "SELECT * FROM users WHERE emailUtilizador = ?";
    
                        $stmt = mysqli_stmt_init($connection);
    
                        if(!mysqli_stmt_prepare($stmt, $sql)){
                            header("Location: ../../../html/index.php?error=sqlerror");
                            exit();
                        }
                        else{
                            mysqli_stmt_bind_param($stmt, "s", $tokenEmail);
                            mysqli_stmt_execute($stmt);
    
                            $result = mysqli_stmt_get_result($stmt);
    
                            if(!$row = mysqli_fetch_assoc($result)){
                                header("Location: ../../../html/index.php?error=bademail");
                                exit();
                            }
                            else{
                                $sql = "UPDATE users SET passwordUtilizador = ? WHERE emailUtilizador = ?";
    
                                $stmt = mysqli_stmt_init($connection);
    
                                if(!mysqli_stmt_prepare($stmt, $sql)){
                                    header("Location: ../../../html/index.php?error=sqlerror");
                                    exit();
                                }
                                else{
                                    $newpwdhash = password_hash($pwd, PASSWORD_DEFAULT);
    
                                    mysqli_stmt_bind_param($stmt, "ss", $newpwdhash, $tokenEmail);
                                    mysqli_stmt_execute($stmt);
    
                                    header("Location: ../../../html/index.php?newpwd=passwordupdated");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else{
        header("Location: ../../../html/index.php");
        exit();
    }
?>