<!-- MySchedule - Carlos Ferreira -->
<?php
    session_start();
    if(isset($_POST['login-submit'])){

        require ('../database.php');

        $mailuid = $_POST['usernameInput'];
        $password = $_POST['passwordInput'];

        if(empty($mailuid) || empty($password)){
            header("Location: ../../../html/index.php?error=emptyfields&modal=myid");
            exit();
            
        }
        else{
            $sql = "SELECT * FROM users WHERE nomeUtilizador = ? OR emailUtilizador = ?;";
            $stmt = mysqli_stmt_init($connection);

            if(!mysqli_stmt_prepare($stmt, $sql)){
                header("Location: ../../../html/index.php?error=sqlerror&modal=myid");
                exit();
            }
            
            else{
                mysqli_stmt_bind_param($stmt, "ss", $mailuid, $mailuid);
                mysqli_stmt_execute($stmt);

                $result = mysqli_stmt_get_result($stmt);

                if($row = mysqli_fetch_assoc($result)){
                    $pwdcheck = password_verify($password,$row['passwordUtilizador']);
                    if($pwdcheck == false){
                        header("Location: ../../../html/index.php?error=wrongpassword&modal=myid");
                        exit();
                    }
                    
                    else if($pwdcheck == true){
                        $_SESSION['userid'] = $row['idUtilizador'];
                        $_SESSION['username'] = $row['nomeUtilizador'];

                        header("Location: ../../../html/actions.php");
                        exit();
                    }
                }
                else{
                    header("Location: ../../../html/index.php?error=nouser&modal=myid");
                    exit();
                }
            }
        }
    }
    else{
        header("Location: ../../../html/index.php");
        exit();
    }
    