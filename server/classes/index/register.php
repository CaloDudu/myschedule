<!-- MySchedule - Carlos Ferreira -->
<?php
    if(isset($_POST['reg-submit'])){

        require ('../database.php');

        $username = $_POST['usernameInputReg'];
        $email = $_POST['emailInputReg'];
        $password = $_POST['passwordInputReg'];
        $passwordRepeat = $_POST['passwordInputReg-repeat'];

        if(empty($username) || empty($email) || empty($password) || empty($passwordRepeat)){
            header("Location: ../../../html/index.php?error=emptyfields&uid=".$username."&mail=".$email."&modal=register");
            exit();
        }
        else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            header("Location: ../../../html/index.php?error=invalidemail&uid=".$username."&modal=register&mail=".$email);
            exit();
        }
        else if($password !== $passwordRepeat){
            header("Location: ../../../html/index.php?error=passwordcheck&uid=".$username."&mail=".$email."&modal=register");
            exit();
        }
        else{
            $sql = "SELECT nomeUtilizador FROM users WHERE nomeUtilizador = ? OR emailUtilizador = ?";
            $stmt = mysqli_stmt_init($connection);

            if(!mysqli_stmt_prepare($stmt,$sql)){
                header("Location: ../../../html/index.php?error=sqlerror&modal=register");
                exit();
            }
            else{
                mysqli_stmt_bind_param($stmt, "ss", $username, $email);
                mysqli_stmt_execute($stmt);

                mysqli_stmt_store_result($stmt);

                $resultCheck = mysqli_stmt_num_rows($stmt);

                if($resultCheck > 0){
                    header("Location: ../../../html/index.php?error=useroremailtaken&uid=".$username."&mail=".$email."&modal=register");
                    exit();
                }
                else{
                    $sql1 = "INSERT INTO users (nomeUtilizador, emailUtilizador, passwordUtilizador) VALUES(?,?,?)";
                    $stmt1 = mysqli_stmt_init($connection);
                    if(!mysqli_stmt_prepare($stmt1,$sql1)){
                        header("Location: ../../../html/index.php?error=sqlerror"."&modal=register");
                        exit();
                    }
                    else{
                        $hashedpwd = password_hash($password, PASSWORD_DEFAULT);

                        mysqli_stmt_bind_param($stmt1, "sss", $username, $email, $hashedpwd);
                        mysqli_stmt_execute($stmt1);

                        header("Location: ../../../html/index.php?signup=sucess");
                        exit();
                    }
                }
            }
        }
    }
    else{
        header("Location: ../../../html/index.php?");
        exit();
    }
?>