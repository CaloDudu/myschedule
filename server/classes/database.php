<!-- MySchedule - Carlos Ferreira -->
<?php
    $servername = "localhost";
    $username = "root";
    $passwordDb = "";
    $database = "myschedule";

    $connection = mysqli_connect($servername,$username,$passwordDb,$database);    
    
    if(!$connection){
        header("Location: ../../html/index.php?error=dberror");
        exit();
    }
    else{
        if(!$connection->set_charset("utf8")){
            header("Location: ../../html/index.php?error=utferror");
            exit();
        }
    }
?>