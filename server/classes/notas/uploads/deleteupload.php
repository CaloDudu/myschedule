<!-- MySchedule - Carlos Ferreira -->
<?php
    session_start();
    require '../../database.php';

    if(isset($_SESSION['userid']) && $_GET['idfile'] && $_GET['path'] && $_GET['ext'] && $_GET['filename'] && $_GET['id_nota']){
        $id_file = $_GET['idfile'];
        $file_path = $_GET['path'];
        $extension = $_GET['ext'];
        $file_name = $_GET['filename'];
        $id_user = $_SESSION['userid'];
        $id_nota = $_GET['id_nota'];
        $session_id = $_SESSION['userid'];
        
        $sql = "DELETE FROM uploads WHERE idNota = ? AND idUtilizador = ? AND id_imagem = ?";
        $stmt = mysqli_stmt_init($connection);

        if(!mysqli_stmt_prepare($stmt, $sql)){
            header("Location: ../../html/actions.php?error=cantpreparestmt");
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt, "iii",$id_nota, $session_id, $id_file);
            mysqli_stmt_execute($stmt);

            header("Location: http://localhost/myschedule/html/actions.php");
            exit();
        }
    }
    else{
        header("Location: http://localhost/myschedule/html/actions.php");
        exit();
    }