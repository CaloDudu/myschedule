<!-- MySchedule - Carlos Ferreira -->
<?php
session_start();

if (isset($_SESSION['userid'])) {

    require("../database.php");

    $id_nota = isset($_GET['id']) ? $_GET['id'] : $_POST['id_copy'];

    $session_id = $_SESSION["userid"];

    $sql = "DELETE FROM notes WHERE idNota = ? AND idUtilizador = ?";
    $stmt = mysqli_stmt_init($connection);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../../html/actions.php?error=cantpreparestmt");
        exit();
    } else {
        mysqli_stmt_bind_param($stmt, "ss", $id_nota, $session_id);
        mysqli_stmt_execute($stmt);

        header("Location: http://localhost/myschedule/html/actions.php");
        exit();
    }
} else {
    header("Location: ./../../../../html/actions.php&error=nosession");
}
?>