<!-- MySchedule - Carlos Ferreira -->
<?php
    session_start();

    if(isset($_SESSION['userid'])){

        require ("../database.php");

        if(isset($_POST['insertNotesBtn'])){
            $session_id = $_SESSION["userid"];

            $titNota = $_POST['titNota'];
            $detalhesNota = $_POST['detalhesNota'];

            if(empty($titNota)){
                header("Location: ../../../html/actions.php?error=emptytit");
                exit();
            }
            else{
                $sql = "INSERT INTO notes(detalhesNota,tituloNota,idUtilizador) VALUES (?,?,?)";
                $stmt = mysqli_stmt_init($connection);
    
                if(!mysqli_stmt_prepare($stmt, $sql)){
                    header("Location: ../../html/actions.php?error=cantpreparestmt");
                    exit();
                }
                else{
                    mysqli_stmt_bind_param($stmt, "sss", $detalhesNota, $titNota, $session_id);
                    mysqli_stmt_execute($stmt);
    
                    header("Location: http://localhost/myschedule/html/actions.php");
                    exit();
                }
            }
        }

    }
    else {
        header("Location: ./../../../../html/actions.php&error=nosession");
    }
?>