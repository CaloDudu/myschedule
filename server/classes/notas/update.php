<!-- MySchedule - Carlos Ferreira -->
<?php
    session_start();

    if(isset($_SESSION['userid'])){

        require ("../database.php");

        if(isset($_POST['update-notes-btn'])){
                
            $session_id = $_SESSION["userid"];
            $titNota = $_POST['titulo'];
            $detalhesNota = $_POST['detalhes'];
            $id_nota = $_POST['id'];

            if(empty($titNota)){
                header("Location: http://localhost/myschedule/html/actions.php");
                exit();
            }
            else{
                $sql = "UPDATE notes SET detalhesNota = ?, tituloNota = ? WHERE idNota = ? AND idUtilizador = ?";
                $stmt = mysqli_stmt_init($connection);
    
                if(!mysqli_stmt_prepare($stmt, $sql)){
                header("Location: /../../../../html/actions.php");
                    }
                else{
                    mysqli_stmt_bind_param($stmt, "ssss", $detalhesNota, $titNota, $id_nota, $session_id);
                    mysqli_stmt_execute($stmt);
    
                    header("Location: http://localhost/myschedule/html/actions.php");
                }
            }

            //Uploads
            $targetDir = "../../../static/uploads/";
            $xdb = "../static/uploads/";
            $allowTypes = array('exe','jpg','png','jpeg','gif','docx','pdf','JPG','PNG','JPEG','GIF','DOCX','PDF','EXE');

            if(!empty(array_filter($_FILES['files']['name']))){
                foreach($_FILES['files']['name'] as $key=>$val){
                    // File upload path
                    $fileName = basename($_FILES['files']['name'][$key]);
                    $temp = explode(".", $fileName);
                    $newfilename = rand(1,1000000) . '.' . end($temp);
                    $targetFilePath = $targetDir . $newfilename;
                    $targetFinali = $xdb . $newfilename;
                    
                    // Validação de extensões
                    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                    if(file_size < 2147483648){
                        if(in_array($fileType, $allowTypes)){
                            if(move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath)){
                                $sql_anexar = "INSERT INTO uploads (file_name, file_name_original,path_imagem, idUtilizador, idNota) VALUES(?,?,?,?,?)";
                                $stmt = mysqli_stmt_init($connection);
                    
                                if(!mysqli_stmt_prepare($stmt, $sql_anexar)){
                                    header("Location: /../../../../html/actions.php");
                                }
                                else{
                                    mysqli_stmt_bind_param($stmt, "sssii", $newfilename, $fileName,$targetFinali, $session_id, $id_nota);
                                    mysqli_stmt_execute($stmt);

                                    header("Location: http://localhost/myschedule/html/actions.php");
                                }
                            }else{
                                header("Location: http://localhost/myschedule/html/actions.php?error=bad_location");
                            }
                        }
                        else{
                            header("Location: http://localhost/myschedule/html/actions.php?error=bad_file_extension");
                        }
                    }else{
                        header("Location: http://localhost/myschedule/html/actions.php?error=2GbfileMax");
                    }
                }
            }else{
                header("Location: http://localhost/myschedule/html/actions.php"); 
            }
        }
        }
    else {
        header("Location: ./../../../../html/actions.php&error=nosession");
    }
?>