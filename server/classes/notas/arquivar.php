<!-- MySchedule - Carlos Ferreira -->
<?php
session_start();

if (isset($_SESSION['userid'])) {

    require("../database.php");

    if (isset($_GET['id'])) {
        $idnota = $_GET['id'];
        $sql = "UPDATE notes SET arquivo = 1 WHERE idNota = ?";
        $stmt = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../../html/actions.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "i", $idnota);
            mysqli_stmt_execute($stmt);

            header("Location: http://localhost/myschedule/html/archive.php");
            exit();
        }
    } else {
        header("Location: ../../../../../html/actions.php");
    }
} else {
    header("Location: ../../../../../html/actions.php");
}
?>