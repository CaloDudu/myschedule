    <?php
        session_start();
        require '../classes/database.php';

        use PHPMailer\PHPMailer\PHPMailer;
        use PHPMailer\PHPMailer\Exception;

        require '../../server/classes/phpmailer/src/Exception.php';
        require '../../server/classes/phpmailer/src/PHPMailer.php';
        require '../../server/classes/phpmailer/src/SMTP.php';

        $now = date("Y-m-d");

        $sql_warning_past = "SELECT * FROM tasks WHERE dataLimiteTask < ? AND status = 0";
        $sql_warning_today = "SELECT * FROM tasks WHERE dataLimiteTask = ? AND status = 0";

        $stmt_warning = mysqli_stmt_init($connection);

        //Notificações: Tarefas com prazo de validade ultrapassado

        if (!mysqli_stmt_prepare($stmt_warning, $sql_warning_past)) {
            header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt_warning, "s", $now);
            mysqli_stmt_execute($stmt_warning);

            $result = mysqli_stmt_get_result($stmt_warning);

            while ($row = $result->fetch_assoc()) {
                $warn_id = $row['idUtilizador'];
                $warn_taskname = $row['nomeTask'];
                $warn_priority = $row['importanciaTask'];

                if ($warn_priority == 3) {
                    $prioridade = 'Importantíssimo';
                } else if ($warn_priority == 2) {
                    $prioridade = 'Posso fazer amanhã';
                } else {
                    $prioridade = 'Ok. Sem preocupações.';
                }

                $sql_get_data = "SELECT emailUtilizador FROM users WHERE idUtilizador = ?";
                $stmt_get_data = mysqli_stmt_init($connection);

                if (!mysqli_stmt_prepare($stmt_get_data, $sql_get_data)) {
                    header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmt");
                    exit();
                } else {
                    mysqli_stmt_bind_param($stmt_get_data, "s", $warn_id);
                    mysqli_stmt_execute($stmt_get_data);

                    $result_email = mysqli_stmt_get_result($stmt_get_data);

                    if ($row_email = mysqli_fetch_assoc($result_email)) {
                        $warn_email = $row_email['emailUtilizador'];

                        //Enviar Email
                        $destino = $warn_email;
                        $subject = "Tens tarefas por concluir";
                        $url = "http://localhost/myschedule/html/index.php";

                        $mensagem = '<p>Tens tarefas por concluir!<br>Sê produtivo! <br> Tarefa a concluir: ' . "$warn_taskname" . ' [Importância: ' . "$prioridade" . ']</p>';
                        $mensagem .= '<p><b>Para entrar na aplicação myschedule clique em </b><a href="' . $url . '">' . $url . '</a></p>';

                        $mail = new PHPMailer();
                        $mail->isSMTP();
                        $mail->SMTPAuth = true;
                        $mail->SMTPSecure = 'ssl';
                        $mail->Host = 'tls://smtp.gmail.com:587';
                        $mail->Port = '465';
                        $mail->isHTML();
                        $mail->Username = 'myschedulebot@gmail.com';
                        $mail->Password = 'myschedule123';
                        $mail->SetFrom('myschedulebot@gmail.com');
                        $mail->Subject = $subject;
                        $mail->Body = $mensagem;
                        $mail->AddAddress($destino);

                        if (!$mail->send()) {
                            echo 'A mensagem não foi enviada.';
                            echo 'Erro: ' . $mail->ErrorInfo;
                        } else {
                        }
                    }
                }
            }
        }

        //Notificações: Tarefas do próprio dia

        if (!mysqli_stmt_prepare($stmt_warning, $sql_warning_today)) {
            header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmt");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt_warning, "s", $now);
            mysqli_stmt_execute($stmt_warning);

            $result = mysqli_stmt_get_result($stmt_warning);

            while ($row = $result->fetch_assoc()) {
                $warn_id = $row['idUtilizador'];
                $warn_taskname = $row['nomeTask'];
                $warn_priority = $row['importanciaTask'];

                if ($warn_priority == 3) {
                    $prioridade = 'Importantíssimo';
                } else if ($warn_priority == 2) {
                    $prioridade = 'Posso fazer amanhã';
                } else {
                    $prioridade = 'Ok. Sem preocupações.';
                }

                $sql_get_data = "SELECT emailUtilizador FROM users WHERE idUtilizador = ?";
                $stmt_get_data = mysqli_stmt_init($connection);

                if (!mysqli_stmt_prepare($stmt_get_data, $sql_get_data)) {
                    header("Location: http://localhost/myschedule/html/todo.php?error=cantpreparestmt");
                    exit();
                } else {
                    mysqli_stmt_bind_param($stmt_get_data, "s", $warn_id);
                    mysqli_stmt_execute($stmt_get_data);

                    $result_email = mysqli_stmt_get_result($stmt_get_data);

                    if ($row_email = mysqli_fetch_assoc($result_email)) {
                        $warn_email = $row_email['emailUtilizador'];

                        //Enviar Email
                        $destino = $warn_email;
                        $subject = "Tens muito a fazer hoje!";
                        $url = "http://localhost/myschedule/html/index.php";

                        $mensagem = '<p>Atenção! <br><b>Hoje</b><br> Tarefa a concluir: ' . "$warn_taskname" . ' [Importância: ' . "$prioridade" . ']</p>';
                        $mensagem .= '<p><b>Para entrar na aplicação myschedule clique em </b><a href="' . $url . '">' . $url . '</a></p>';

                        $mail = new PHPMailer();
                        $mail->isSMTP();
                        $mail->SMTPAuth = true;
                        $mail->SMTPSecure = 'ssl';
                        $mail->Host = 'tls://smtp.gmail.com:587';
                        $mail->Port = '465';
                        $mail->isHTML();
                        $mail->Username = 'myschedulebot@gmail.com';
                        $mail->Password = 'myschedule123';
                        $mail->SetFrom('myschedulebot@gmail.com');
                        $mail->Subject = $subject;
                        $mail->Body = $mensagem;
                        $mail->AddAddress($destino);

                        if (!$mail->send()) {
                            echo 'A mensagem não foi enviada.';
                            echo 'Erro: ' . $mail->ErrorInfo;
                        } else {
                            header("Location: http://localhost/MySchedule/html/index.php?modal=emailsent");
                        }
                    }
                }
            }
        }
