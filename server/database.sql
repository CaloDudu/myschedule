CREATE DATABASE myschedule;

CREATE TABLE `users` (
  `idUtilizador` int(11) NOT NULL,
  `nomeUtilizador` varchar(100) NOT NULL,
  `emailUtilizador` varchar(255) NOT NULL,
  `passwordUtilizador` varchar(100) NOT NULL,
  PRIMARY KEY (`idUtilizador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `labels` (
  `idLabel` int(11) NOT NULL,
  `nomeLabel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idUtilizador` int(11) NOT NULL,
  PRIMARY KEY (`idLabel`),
  CONSTRAINT `labelsusers` FOREIGN KEY (`idUtilizador`) REFERENCES users(`idUtilizador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `notes` (
  `idNota` int(20) NOT NULL,
  `detalhesNota` text COLLATE utf8_unicode_ci NOT NULL,
  `tituloNota` text COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` int(15) NOT NULL,
  `idUtilizador` int(11) NOT NULL,
  PRIMARY KEY (`idNota`),
  CONSTRAINT `notesusers` FOREIGN KEY (`idUtilizador`) REFERENCES users(`idUtilizador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tasks` (
  `idTask` int(11) NOT NULL,
  `nomeTask` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dataLimiteTask` date NOT NULL,
  `importanciaTask` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `idUtilizador` int(11) NOT NULL,
  PRIMARY KEY (`idTask`),
  CONSTRAINT `tasksusers` FOREIGN KEY (`idUtilizador`) REFERENCES users(`idUtilizador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tokens` (
  `pwdResetId` int(11) NOT NULL,
  `pwdResetEmail` text COLLATE utf8_unicode_ci NOT NULL,
  `pwdResetSelector` text COLLATE utf8_unicode_ci NOT NULL,
  `pwdResetToken` longtext COLLATE utf8_unicode_ci NOT NULL,
  `pwdResetExpires` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY(`pwdResetId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `uploads` (
  `id_imagem` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name_original` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idUtilizador` int(11) NOT NULL,
  `idNota` int(20) NOT NULL,
  PRIMARY KEY (`id_imagem`),
  CONSTRAINT `uploadsusers` FOREIGN KEY (`idUtilizador`) REFERENCES users(`idUtilizador`),
  CONSTRAINT `uploadsnotes` FOREIGN KEY (`idNota`) REFERENCES notes(`idNota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `assoclabels` (
  `idAssoc` int(11) NOT NULL,
  `idLabel` int(11) NOT NULL,
  `idNota` int(11) NOT NULL,
  `idUtilizador` int(11) NOT NULL,
  PRIMARY KEY (`idAssoc`),
  CONSTRAINT `assoclabelsuser` FOREIGN KEY(`idUtilizador`) REFERENCES users(`idUtilizador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `sharestasks` (
  `idPartilhaTask` int(11) NOT NULL,
  `idTask` int(11) NOT NULL,
  `idOrigem` int(11) NOT NULL,
  `emailDestino` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resultado_trans` int(11) NOT NULL,
  PRIMARY KEY (`idPartilhaTask`),
  CONSTRAINT `sharesktaskstasks` FOREIGN KEY (`idTask`) REFERENCES tasks(`idTask`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `shares` (
  `idPartilha` int(11) NOT NULL,
  `idNota` int(11) NOT NULL,
  `idOrigem` int(11) NOT NULL,
  `emailDestino` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resultado` int(11) NOT NULL,
  PRIMARY KEY (`idPartilha`),
  CONSTRAINT `sharesnotes` FOREIGN KEY (`idNota`) REFERENCES notes(`idNota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

