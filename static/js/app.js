    //MySchedule - Carlos Ferreira

    $(() => {
        let chips = new Array();

        $('#slide-out').sidenav();

        $('#olaola').modal();

        $('.collapsible').collapsible();

        $('.tooltipped').tooltip();

        $('#sidenav_right_side').sidenav({
            edge: 'right'
        });

        $('#sidenav_right_side_labels').sidenav({
            edge: 'right'
        });

        $('.datepicker').datepicker({
            i18n: {
                months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                monthsShort: ["Jan", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dez"],
                weekdays: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
                weekdaysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                weekdaysAbbrev: ["D", "S", "T", "Q", "Q", "S", "S"],
                cancel: 'Cancelar',
                clear: 'Limpar',
                done: 'Ok',
            },
            dateFormat: "yyyy-mm-dd"
        });

        $('.dropdown-trigger').dropdown();

        $('select').formSelect();

        $('#sidenav_right_side').sidenav('open');

        $('.chips').chips();

        $('#form_share').submit(function (e) {
            let data = '';
            M.Chips.getInstance($('.chips')).chipsData.forEach(el => data += el.tag + " ");
            $('#storage').html(data);

            $('input[name=storage]').val(data);
        });
        
        $('#form_share_task').submit(function (e) {
            let data = '';
            M.Chips.getInstance($('.chips')).chipsData.forEach(el => data += el.tag + " ");
            $('#storage').html(data);

            $('input[name=storage]').val(data);
        });

        $('.chips').keyup((e) => {
            if (e.keyCode == 13 || e.keyCode == 32) {
                let instance = M.Chips.getInstance($('.chips'));
                instance.addChip({
                    tag: $('.chips input').val()
                });

                $('.chips input').val('');
            }
        })
    });

    function getDetails(id){
        var detailsName = $('#'+id+'nomestorageshares').text();
        var detailsEmail = $('#'+id+'emailstorageshares').text();

        $('#h4').html(detailsName);
        $('#p').html("Email: "+detailsEmail);
    }

    function openMyAccount(){
        let modalcard = $('#aminhaconta');
        let username = $('#nomestorage').val();
        let email = $('#emailstorage').val();

        $('#nome').attr("placeholder",username);
        $('#icon_telephone').attr("placeholder", email);

        UIkit.modal(modalcard).show();
    }

    function openModalEditTask(l, id) {
        let modalcard = $('#modal-card-task');
        let taskname = $('#' + id + ' #name_task');
        let datetime = $('#' + id + ' #datetime_write_task').val();
        let prioridade = $('#' + id + ' #priority_task_to_write').text();

        prioridade = parseInt(prioridade);

        $('#id1').val(id);
        $("#insertdatetime_task").attr("placeholder", datetime);
        $('#name_task_write').html(taskname.text());

        $('#changePriority').empty();

        if (prioridade == 1) {
            $(`#changePriority`).append(`<option selected value="1">Pouco Importante</option>
                <option value="2">OK. Faço Amanhã</option>
                <option value="3">Importantíssimo</option>`);
        } else if (prioridade == 2) {
            $(`#changePriority`).append(`<option value="1">Pouco Importante</option>
                <option selected value="2">OK. Faço Amanhã</option>
                <option value="3">Importantíssimo</option>`);
        } else {
            $(`#changePriority`).append(`<option value="1">Pouco Importante</option>
                <option value="2">OK. Faço Amanhã</option>
                <option selected value="3">Importantíssimo</option>`);
        }
        $('select').formSelect();
        UIkit.modal(modalcard).show();
    }

    function openModal(id) {
        console.log(id);
        let modalcard = $('#modal-card');
        let notap = $('#' + id + ' #quack');
        let corponota = $('#' + id + ' h3');

        $('#titulo1').text(corponota.text());
        $('#detalhes1').html(notap.text());
        $('#id1').val(id);
        $('#id2').val(id);
        $('#id_anexar').val(id);
        $('#id_get_note').val(id);

        $("#div_path").empty();

        $("div #img"+id).each((index, value) => {
            let xyz = $(value).attr('value');
            let id_file = $(value).attr('name');
            let file_name_original = $(value).attr('value_original');

            var str = xyz;
            var extension = str.split('.').pop();
            var file_name = str.split('/').pop();

            if (extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'gif' || extension == 'GIF' || extension == 'JPG' || extension == 'JPEG' || extension == 'PNG') {
                $("#div_path").append(`<div class="col s8" id="file${id_file}"> <img class="materialboxed" width=170 src="${xyz}"></div>`);
            } else {
                if (extension == 'docx' || extension == 'DOCX') {
                    $("#div_path").append(`<div class="col s8" id="file${id_file}"><img src="../static/imagens/docx.png" width=100></div>`);
                } else if (extension == 'pdf' || extension == 'PDF') {
                    $("#div_path").append(`<div class="col s8" id="file${id_file}"><img src="../static/imagens/pdf.png" width=100></div>`);
                }
            }

            $(`#file${id_file}`).append(`<div class="uk-width-small uk-child-width-1-2@m" uk-dropdown="pos: bottom-justify; delay-hide: 10">
            <ul class="uk-nav uk-dropdown-nav">
                <li>
                <a href="${xyz}" download>
                    <i class="material-icons">publish</i>Transferir ${file_name_original}
                </a>
                </li>
            <ul class="uk-nav uk-dropdown-nav">
            <li>
            <a href="../server/classes/notas/uploads/deleteupload.php?path=${xyz}&idfile=${id_file}&ext=${extension}&filename=${file_name}&id_nota=${id}">
            <i class="material-icons">delete</i>Eliminar</a></li></ul></div>`);
            $(`#file${id_file}`).hover(() => {
                UIkit.dropdown($(`#file${id_file}`).Children()).show();

            });
        });
        UIkit.modal(modalcard).show();
        $('.materialboxed').materialbox();
    }

    function openModal_Create_Label(id) {
        let modal_create_label = $('#create_label_modal');
        UIkit.modal(modal_create_label).show();
    }

    function openModalEditNameLabel(id) {
        $('#get_id_label').val(id);
        let modal_edit_label = $('#edit_label_modal');
        UIkit.modal(modal_edit_label).show();
    }

    function openModalLabels_Assoc(id) {
        $('#id_nota').val(id);
        $('#get_id_to_assoc').val(id);

        $("#append_labels_to_assoc").empty();

        $(`div[id_idnota='nota${id}']`).each((index, value) => {
            let id_label = $(value).attr('id_label');
            let name_label = $(value).attr('value_name_label');
            let checked = "";

            // !! - retorna bool
            if (!!$(value).attr('ativo'))
                checked = "checked";
            $("#append_labels_to_assoc").append(`<p>
                <label>
                    <input name="etiqueta_${id_label}" class="filled-in" value="${id_label}" type="checkbox" ${checked}/>
                    <span id=${id_label}>${name_label}</span>
                </label>
            </p>`);
        });

        let modal_label_assoc = $('#assoc_label_to_note');
        UIkit.modal(modal_label_assoc).show();
    }

    function openModalShare(id) {
        $('#id_share_modal').val(id);

        let modalsharecard = $('#modal-share-card');
        UIkit.modal(modalsharecard).show();
    }

    function openModalShareTask(id) {
        $('#id_share_task').val(id);

        let modalsharecard = $('#modal-share-task-card');
        UIkit.modal(modalsharecard).show();
    }

    function openModalNotesShared(id) {
        let modalcard = $('#modal-shared-notes');
        let notap = $('#' + id + ' #quack');
        let corponota = $('#' + id + ' h3');

        $('#titulo2').text(corponota.text());
        $('#detalhes2').html(notap.text());
        $('#id1').val(id);
        $('#id2').val(id);
        $('#id_anexar').val(id);
        $('#id_get_note').val(id);

        $("#div_path_shares").empty();

        $("div #img" + id).each((index, value) => {

            let xyz = $(value).attr('value');
            let id_file = $(value).attr('name');
            let file_name_original = $(value).attr('value_original');

            var str = xyz;
            var extension = str.split('.').pop();
            var file_name = str.split('/').pop();

            if (extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'gif' || extension == 'GIF' || extension == 'JPG' || extension == 'JPEG' || extension == 'PNG') {
                $("#div_path_shares").append(`<div class="col s8" id="file${id_file}"> <img class="materialboxed" width=170 src="${xyz}"></div>`);
            } else {
                if (extension == 'docx' || extension == 'DOCX') {
                    $("#div_path_shares").append(`<div class="col s8" id="file${id_file}"><img src="../static/imagens/docx.png" width=100></div>`);
                } else if (extension == 'pdf' || extension == 'PDF') {
                    $("#div_path_shares").append(`<div class="col s8" id="file${id_file}"><img src="../static/imagens/pdf.png" width=100></div>`);
                }
            }
            $(`#file${id_file}`).append(`<div class="uk-width-small uk-child-width-1-2@m" uk-dropdown="pos: bottom-justify; delay-hide: 10">
            <ul class="uk-nav uk-dropdown-nav">
            <li>
                <a href="${xyz}" download>
                    <i class="material-icons">publish</i>Transferir ${file_name_original}
                </a>
                </li>
            </div>`);
            $(`#file${id_file}`).hover(() => {
                UIkit.dropdown($(`#file${id_file}`).Children()).show();
            });
        });
        $('.materialboxed').materialbox();
        UIkit.modal(modalcard).show();
    }