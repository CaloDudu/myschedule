        <!-- MySchedule - Carlos Ferreira -->
        <?php session_start(); ?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>myschedule</title>
            <link rel="icon" href="../static/imagens/logo_2.png" type="image/ico">

            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
            <link rel="stylesheet" href="../static/css/index.css">
            <link rel="stylesheet" href="../static/css/uikit/uikit.css">
            <link rel="stylesheet" href="../static/css/materialize/css/materialize.css">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

            <script src="../static/js/uikit/uikit.min.js"></script>
            <script src="../static/js/uikit/uikit-icons.min.js"></script>
            <style>
                nav,
                main,
                footer {
                    padding-right: 230px;
                }

                @media only screen and (max-width : 992px) {

                    nav,
                    main,
                    footer {
                        padding-right: 0;
                    }
                }

                #meugrandebutton {
                    right: 270px !important;
                }
            </style>
        </head>

        <body class="white">
            <header>
                <div class="navbar">
                    <nav class="white" style="padding: 0 0 75px 0">
                        <div class="nav-wrapper">
                            <div class="container-fluid">
                                <a href="#" data-target="slide-out" class="black-text show-on-medium-and-up sidenav-trigger left"><i class="material-icons">menu</i></a>
                                <img width="47" height="47" src="../static/imagens/logo_2.png">
                                <a class="logo" href="http://localhost/myschedule/html/actions.php">myschedule</a>
                            </div>
                        </div>
                    </nav>
                </div>

                <ul id="slide-out" class="sidenav"><br>
                    <li><a href="http://localhost/MySchedule/html/actions.php"><i class="material-icons">note</i>Notas</a></li>
                    <li>
                        <div class="divider"></div>
                    </li>
                    <li><a href="http://localhost/MySchedule/html/todo.php"><i class="material-icons">schedule</i>Tarefas</a></li>
                    <li>
                        <div class="divider"></div>
                    </li>
                    <li><a onClick="openMyAccount();"><i class="material-icons">person</i>A Minha Conta</a></li>

                    <form action="../server/classes/index/logout.php" method="POST">
                        <input type="submit" class="uk-button uk-button-primary round" style="margin: 65px; margin-bottom: -450px;" name="log-out" value="Sign Out">
                    </form>
                </ul>

                <ul id="sidenav_right_side" class="sidenav sidenav-fixed" style="width:240px !important;"><br>
                    <li>
                        <h6 class="center"><i class="material-icons">more_horiz</i></h6>
                    </li>
                    <li><a href="http://localhost/MySchedule/html/archive.php"><i class="material-icons">backup</i>Arquivo</a></li>
                    <li>
                        <div class="divider"></div>
                    </li>
                    <li><a href="http://localhost/MySchedule/html/notificacoes.php"><i class="material-icons">sms</i>Notificações</a></li>
                    <li>
                        <div class="divider"></div>
                    </li>
                    <li><a href="#"><i class="material-icons">label</i>Etiquetas</a></li>

                    <?php
                    require('../server/classes/database.php');
                    $session_id = $_SESSION['userid'];

                    $sql_get_labels_sidebar = "SELECT * FROM labels WHERE idUtilizador = ?;";
                    $stmt_get_labels_sidebar = mysqli_stmt_init($connection);

                    if (!mysqli_stmt_prepare($stmt_get_labels_sidebar, $sql_get_labels_sidebar)) {
                        header("Location: ../../../html/actions.php?error=cantpreparestmt");
                        exit();
                    } else {
                        mysqli_stmt_bind_param($stmt_get_labels_sidebar, "s", $session_id);
                        mysqli_stmt_execute($stmt_get_labels_sidebar);

                        $result_get_labels_sidebar = mysqli_stmt_get_result($stmt_get_labels_sidebar);
                        while ($row_get_labels_sidebar = $result_get_labels_sidebar->fetch_array()) {
                            $get_id_label_sidebar = $row_get_labels_sidebar['idLabel'];
                            $get_name_label_sidebar = $row_get_labels_sidebar['nomeLabel'];
                            ?>
                            <li style="padding-left: 45px !important;">
                                <ul class="collapsible expandable">
                                    <li id=<?php echo $get_id_label_sidebar; ?>>
                                        <div class="collapsible-header"><i class="material-icons">label</i><?php echo $get_name_label_sidebar ?></div>
                                        <div class="collapsible-body" style="padding-left: 35px !important;">
                                            <a href="../server/classes/labels/deletelabel.php?id=<?php echo $get_id_label_sidebar; ?>" class="black-text"><i class="material-icons" style="vertical-align: sub !important;">delete</i>&nbsp;&nbsp;&nbsp;Eliminar</a><br>
                                            <a id=<?php echo $get_id_label_sidebar; ?> href="#" class="black-text" onClick="openModalEditNameLabel(this.id);"><i class="material-icons" style="vertical-align: sub !important;">edit</i>&nbsp;&nbsp;&nbsp;Renomear</a><br>
                                            <a id=<?php echo $get_id_label_sidebar; ?> href="../html/labels.php?label=<?php echo $get_id_label_sidebar ?>" class="black-text""><i class=" material-icons" style="vertical-align: sub !important;">visibility</i>&nbsp;&nbsp;&nbsp;Ver todos</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        <?php
                    }
                }
                ?>
                    <li><a href="#" onClick="openModal_Create_Label(this.id);" id="create_label"><i class="material-icons">control_point</i>Criar etiqueta</a></li>
                </ul>
                
                <a id="meugrandebutton" href="#" class="float tooltipped" data-position="top" data-tooltip="Cria notas!" uk-toggle="target: #create-notes"><i class="fa fa-plus my-float"></i></a>

            </header>

            <main>
                <?php
                if (isset($_SESSION['userid'])) { ?>
                    <!-- Nota Partilhada Modal -->
                    <div id="sent_shared_note" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body" style="border-radius: 10px">
                            <h6 style="font-weight: bold;">A sua nota foi partilhada</h6>
                            <br>
                            <img width="300" height="300" src="../static/imagens/transfer_files.png">
                        </div>
                    </div>

                    <?php
                    if (isset($_GET['modal'])) {
                        $modal = $_GET['modal'];
                        if ($modal == "sentnote_sucess") {
                            echo ("<script>UIkit.modal($('#sent_shared_note')).show();</script>");
                        } else if ($modal == "changedprofile") {
                            echo ("<script>UIkit.modal($('#profileupdated')).show();</script>");
                        }

                        if (isset($_GET['error'])) {
                            echo ("<script>M.toast({html: 'Campos inválidos', classes: 'rounded'});</script>");
                        }
                    }
                    ?>

                    <?php
                    require '../server/classes/database.php';
                    $session_id = $_SESSION['userid'];
                    //Receber todos os dados do utilizador
                    $sql_get_data_user = "SELECT * FROM users WHERE idUtilizador = ?";
                    $stmt_get_data_user = mysqli_stmt_init($connection);

                    if (!mysqli_stmt_prepare($stmt_get_data_user, $sql_get_data_user)) {
                        header("Location: ../../todo.php");
                        exit();
                    } else {
                        mysqli_stmt_bind_param($stmt_get_data_user, "s", $session_id);
                        mysqli_stmt_execute($stmt_get_data_user);

                        $result = mysqli_stmt_get_result($stmt_get_data_user);

                        if ($row_get_data_user = mysqli_fetch_assoc($result)) {
                            $username = $row_get_data_user['nomeUtilizador'];
                            $email = $row_get_data_user['emailUtilizador'];
                            ?>
                            <input type="hidden" name="nomestorage" id="nomestorage" value="<?php echo $username; ?>">
                            <input type="hidden" name="emailstorage" id="emailstorage" value="<?php echo $email; ?>">

                        <?php
                    } else {
                        echo "<script>console.log('asd');</script>";
                    }
                }
                ?>

                    <!-- Modal - A minha conta -->
                    <div id="aminhaconta" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
                            <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">A Minha Conta</h4>
                            <div class="row">
                                <div class="col s12 m6">
                                    <div class="row">
                                        <form class="col s12" method="POST" action="./../server/classes/profile/changeprofile.php">
                                            <br>
                                            <div class="row">
                                                <div class="input-field col s9">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input id="nome" type="text" class="validate" name="nome">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s9">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="icon_telephone" type="text" class="validate" name="icon_telephone">
                                                </div>
                                            </div>
                                            <input style="margin-left: 298px !important;" type="submit" class="uk-button uk-button-default" name="changeprofile" value="Guardar">
                                        </form>
                                    </div>
                                </div><br>
                                <div class="col s12 m6">
                                    <img src="./../static/imagens/profilepic.png" style="width: 270px !important;">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal - Cards -->
                    <div id="modal-card" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body" style="width:850px !important;">
                            <div class="row">
                                <div class="col m6 s6 l6">
                                    <div class="row">
                                        <form class="col s12" method="POST" action="./../server/classes/notas/update.php" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="uk-margin">
                                                        <textarea class="uk-textarea" id="titulo1" name="titulo" style="height: 40px; font-weight: bold; font-size: 18px;"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <div class="uk-margin">
                                                        <textarea class="uk-textarea" id="detalhes1" name="detalhes" style="height: 150px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="id_anexar" name="id_anexar">
                                            <div class="uk-margin" uk-margin>
                                                <div uk-form-custom="target: true">
                                                    <input type="file" name="files[]" multiple>
                                                    <input class="uk-input uk-form-width-medium" type="text" placeholder="Anexa Ficheiros!" disabled>
                                                </div>
                                                <button type="submit" class="uk-button uk-button-default" name="update-notes-btn">Guardar</button>
                                            </div>
                                            <input type="hidden" id="id1" name="id">
                                        </form>
                                    </div>
                                </div>
                                <div class="col m6 s6 l6"><br><br>
                                    <div class="row">
                                        <div class="col s12">
                                            <div class="row" id="div_path"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal - Shared - Cards -->
                    <div id="modal-shared-notes" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body" style="width:850px !important;">
                            <div class="row">
                                <div class="col m6 s6 l6">
                                    <div class="row">
                                        <form class="col s12" method="POST" action="./../server/classes/notas/update.php" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="uk-margin">
                                                        <textarea class="uk-textarea" id="titulo2" name="titulo" style="height: 40px; font-weight: bold; font-size: 18px;"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <div class="uk-margin">
                                                        <textarea class="uk-textarea" id="detalhes2" name="detalhes" style="height: 150px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="id_anexar" name="id_anexar">
                                            <input type="hidden" id="id1" name="id">
                                        </form>
                                    </div>
                                </div>
                                <div class="col m6 s6 l6"><br><br>
                                    <div class="row">
                                        <div class="col s12">
                                            <div class="row" id="div_path_shares">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Mostrar Notas -->
                    <div class="row">
                        <?php
                        require('./../server/classes/database.php');

                        $session_id = $_SESSION["userid"];
                        $sql = "SELECT * FROM notes WHERE idUtilizador = ? AND arquivo = 0";
                        $stmt = mysqli_stmt_init($connection);

                        if (!mysqli_stmt_prepare($stmt, $sql)) {
                            exit();
                        } else {
                            mysqli_stmt_bind_param($stmt, "s", $session_id);
                            mysqli_stmt_execute($stmt);

                            $result = mysqli_stmt_get_result($stmt);

                            while ($row = $result->fetch_array()) {

                                $idnota = $row["idNota"];

                                //Upload Anexos
                                $stmt_select_anexos = mysqli_stmt_init($connection);
                                $sql_select_anexos = "SELECT * FROM uploads WHERE idNota = ? AND idUtilizador = ?";

                                if (!mysqli_stmt_prepare($stmt_select_anexos, $sql_select_anexos)) {
                                    echo "erro anexos";
                                } else {
                                    mysqli_stmt_bind_param($stmt_select_anexos, "ii", $idnota, $session_id);
                                    mysqli_stmt_execute($stmt_select_anexos);
                                    $result_anexos = mysqli_stmt_get_result($stmt_select_anexos);

                                    $count_num_rows = $result_anexos->num_rows;

                                    while ($row1 = $result_anexos->fetch_array()) {
                                        $array_paths = $row1['path_imagem'];
                                        $file_name_original_get_php = $row1['file_name_original'];
                                        $id_img = $row1['id_imagem'];
                                        echo "<div id=img" . $idnota . " name=$id_img value=" . $array_paths . " value_original = " . $file_name_original_get_php . "></div>";
                                    }
                                }

                                //Associar Notas a Labels - Mover variáveis para modal de assoc

                                $session_id = $_SESSION['userid'];

                                $sql_get_labels_1 = "SELECT * FROM labels WHERE idUtilizador = ?;";
                                $stmt_get_labels_1 = mysqli_stmt_init($connection);

                                if (!mysqli_stmt_prepare($stmt_get_labels_1, $sql_get_labels_1)) {
                                    header("Location: ../../../html/actions.php?error=cantpreparestmt");
                                    exit();
                                } else {
                                    mysqli_stmt_bind_param($stmt_get_labels_1, "s", $session_id);
                                    mysqli_stmt_execute($stmt_get_labels_1);

                                    $result_get_labels_1 = mysqli_stmt_get_result($stmt_get_labels_1);

                                    while ($row_labels = $result_get_labels_1->fetch_array()) {
                                        $id_label = $row_labels['idLabel'];
                                        $name_label = $row_labels['nomeLabel'];

                                        $sql_select_labels_to_assoc = "SELECT * FROM assoclabels WHERE idNota = ? AND idUtilizador = ?";
                                        $stmt_select_labels_to_assoc = mysqli_stmt_init($connection);

                                        if (!mysqli_stmt_prepare($stmt_select_labels_to_assoc, $sql_select_labels_to_assoc)) {
                                            echo "erro anexos";
                                        } else {
                                            mysqli_stmt_bind_param($stmt_select_labels_to_assoc, "ss", $idnota, $session_id);
                                            mysqli_stmt_execute($stmt_select_labels_to_assoc);

                                            $result_labels_to_assoc = mysqli_stmt_get_result($stmt_select_labels_to_assoc);
                                            $count_rows_labels_assoc = $result_labels_to_assoc->num_rows;

                                            $existe = 0;
                                            if ($count_rows_labels_assoc > 0) {
                                                while ($row_select_labels_to_assoc = $result_labels_to_assoc->fetch_array()) {
                                                    $id_label_to_assoc = $row_select_labels_to_assoc['idLabel'];
                                                    if ($id_label_to_assoc == $id_label) {
                                                        $existe = 1;
                                                    }
                                                }
                                                if ($existe == 1) {
                                                    echo "<div id_idnota=nota" . $idnota . " id_label= $id_label_to_assoc value_name_label = " . $name_label . " ativo = 1></div>";
                                                } else {
                                                    echo "<div id_idnota=nota" . $idnota . " id_label= $id_label value_name_label = " . $name_label . "></div>";
                                                }
                                            } else {
                                                echo "<div id_idnota=nota" . $idnota . " id_label= $id_label value_name_label = " . $name_label . "></div>";
                                            }
                                        }
                                    }
                                }
                                ?>

                                <div class="col s12 m3 l3" style="margin-top: 30px">
                                    <div>
                                        <div class="uk-card uk-card-default">
                                            <div class="uk-card-body" id="<?php echo ($idnota); ?>">

                                                <h3 class="uk-card-title"><?php echo $row["tituloNota"]; ?></h3>

                                                <?php $stringcopia = $row["detalhesNota"];
                                                $string = strlen($row["detalhesNota"]) > 255 ? substr($row["detalhesNota"], 0, 255) . " ..." : $row["detalhesNota"]; ?>
                                                <p><?php echo $string; ?></p>

                                                <span id="getId" style="display: none;"><?php echo ($idnota); ?></span>
                                                <p id="quack" style="display:none"><?php echo $stringcopia; ?></p>

                                            </div>
                                            <div class="uk-card-footer">
                                                <div class="uk-inline left" style="font-size: 13px !important;">
                                                    <!-- Informação sobre anexos -->
                                                    <?php
                                                    if ($count_num_rows == 0) {
                                                        echo "";
                                                    } else {
                                                        if ($count_num_rows == 1) {
                                                            echo "(" . $count_num_rows . " Anexo)";
                                                        } else {
                                                            echo "(" . $count_num_rows . " Anexos)";
                                                        }
                                                    }
                                                    $sql_select_labels_footer = "SELECT labels.nomeLabel, assoclabels.idNota,assoclabels.idUtilizador FROM
                                                    assoclabels INNER JOIN
                                                    labels On assoclabels.idLabel = labels.idLabel
                                                    WHERE
                                                    assoclabels.idNota = ? 
                                                    AND
                                                    assoclabels.idUtilizador = ?";

                                                    $stmt_select_labels_footer = mysqli_stmt_init($connection);

                                                    if (!mysqli_stmt_prepare($stmt_select_labels_footer, $sql_select_labels_footer)) {
                                                        echo "erro anexos";
                                                    } else {
                                                        mysqli_stmt_bind_param($stmt_select_labels_footer, "ss", $idnota, $session_id);
                                                        mysqli_stmt_execute($stmt_select_labels_footer);

                                                        $result_labels_to_footer = mysqli_stmt_get_result($stmt_select_labels_footer);
                                                        $count_rows_labels_footer = $result_labels_to_footer->num_rows;

                                                        if ($count_rows_labels_footer > 0) {
                                                            while ($row_labels_footer = $result_labels_to_footer->fetch_array()) {
                                                                $name_label_footer = $row_labels_footer['nomeLabel'];
                                                                echo " (" . $name_label_footer . ")";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <div class="uk-inline right">
                                                    <i class="material-icons">more_vert</i>
                                                    <div uk-dropdown="pos: right-left; duration: 100">
                                                        <ul class="uk-nav uk-dropdown-nav">
                                                            <li> <a href="../server/classes/notas/delete.php?id=<?php echo ($idnota); ?>"><i class="material-icons" style="vertical-align: sub !important;">delete_outline</i> Eliminar</a></li>
                                                            <li> <a href="../server/classes/notas/arquivar.php?id=<?php echo ($idnota); ?>"> <i class="material-icons" style="vertical-align: sub !important;">backup</i> Arquivar</a></li>
                                                            <li id="<?php echo ($idnota); ?>" onClick="openModal(this.id);"><a href="#"> <i class="material-icons" style="vertical-align: sub !important;">edit</i> Editar</a></li>
                                                            <li id="<?php echo ($idnota); ?>" onClick="openModalShare(this.id);"><a href="#"> <i class="material-icons" style="vertical-align: sub !important;">import_export</i> Partilhar</a></li>
                                                            <li id="<?php echo ($idnota); ?>" onClick="openModalLabels_Assoc(this.id);"><a href="#"><i class="material-icons" style="vertical-align: sub !important;">label</i> Etiquetas</a></li>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            <?php
                        }
                    }
                    ?>
                    </div><br>

                    <div class="divider"></div>

                    <!-- Notas Partilhadas -->
                    <div class="row">
                        <?php
                        $session_id = $_SESSION["userid"];

                        //Receber Email de User
                        $sql_get_email = "SELECT * FROM users WHERE idUtilizador = ?";

                        if (!mysqli_stmt_prepare($stmt, $sql_get_email)) {
                            echo "erro_shares";
                        } else {
                            mysqli_stmt_bind_param($stmt, "i", $session_id);
                            mysqli_stmt_execute($stmt);
                            $result = mysqli_stmt_get_result($stmt);

                            if ($row = mysqli_fetch_assoc($result)) {
                                $email_utilizador = $row['emailUtilizador'];
                            }
                        }

                        $email_utilizador = $row['emailUtilizador'];

                        //Verificar rows de partilha com resultado = 1(aceite) e emaildestino = email_user
                        $sql_shares = "SELECT * FROM shares WHERE emailDestino = ? AND resultado = 1 ORDER BY idPartilha";

                        if (!mysqli_stmt_prepare($stmt, $sql_shares)) {
                            exit();
                        } else {
                            mysqli_stmt_bind_param($stmt, "s", $email_utilizador);
                            mysqli_stmt_execute($stmt);

                            $result = mysqli_stmt_get_result($stmt);

                            while ($row1 = $result->fetch_array()) {

                                $id_nota_shares = $row1["idNota"];

                                $sql_get_data_notes = "SELECT * FROM notes WHERE idNota = ?";

                                if (!mysqli_stmt_prepare($stmt, $sql_get_data_notes)) {
                                    echo "erro_get_data_notes";
                                } else {
                                    mysqli_stmt_bind_param($stmt, "i", $id_nota_shares);
                                    mysqli_stmt_execute($stmt);

                                    $resultado = mysqli_stmt_get_result($stmt);

                                    while ($row = $resultado->fetch_array()) {

                                        $stmt_select_anexos = mysqli_stmt_init($connection);
                                        $sql_select_anexos = "SELECT * FROM uploads WHERE idNota = ?";

                                        if (!mysqli_stmt_prepare($stmt_select_anexos, $sql_select_anexos)) {
                                            echo "erro anexos";
                                        } else {
                                            mysqli_stmt_bind_param($stmt_select_anexos, "i", $id_nota_shares);
                                            mysqli_stmt_execute($stmt_select_anexos);
                                            $result_anexos = mysqli_stmt_get_result($stmt_select_anexos);

                                            while ($row1 = $result_anexos->fetch_array()) {
                                                $file_name_original_get_php = $row1['file_name_original'];
                                                $count_num_rows = $result_anexos->num_rows;
                                                $array_paths = $row1['path_imagem'];
                                                $id_img = $row1['id_imagem'];
                                                echo "<script>console.log('" . $array_paths . "');</script>";
                                                echo "<div id=img" . $id_nota_shares . " name=$id_img value=" . $array_paths . " value_original = " . $file_name_original_get_php . "></div>";
                                            }
                                        }

                                        ?>

                                        <div class="col s12 m3 l3" style="margin-top: 30px">
                                            <div>
                                                <div class="uk-card uk-card-default">
                                                    <div class="uk-card-body" id="<?php echo ($id_nota_shares); ?>" onClick="openModalNotesShared(this.id);">
                                                        <h3 class="uk-card-title"><?php echo $row["tituloNota"]; ?></h3>
                                                        <?php $stringcopia = $row["detalhesNota"];
                                                        $string = strlen($row["detalhesNota"]) > 255 ? substr($row["detalhesNota"], 0, 255) . " ..." : $row["detalhesNota"]; ?>
                                                        <p><?php echo $string; ?></p>
                                                        <span id="getId" style="display: none;"><?php echo ($id_nota_shares); ?></span>
                                                        <p id="quack" style="display:none"><?php echo $stringcopia; ?></p>
                                                    </div>
                                                    <div class="row right">
                                                        <div class="col s12">
                                                            <div class="row right" id="div_path"></div>
                                                        </div>
                                                    </div>


                                                    <div class="uk-card-footer">
                                                        <div class="uk-inline left" style="font-size: 13px !important;">
                                                            <!-- Informação sobre anexos -->
                                                            <?php
                                                            if (isset($count_num_rows)) {
                                                                if ($count_num_rows == 0) {
                                                                    echo "";
                                                                } else {
                                                                    if ($count_num_rows == 1) {
                                                                        echo "(" . $count_num_rows . " Anexo)";
                                                                    } else {
                                                                        echo "(" . $count_num_rows . " Anexos)";
                                                                    }
                                                                }
                                                            } else { }
                                                            ?>
                                                        </div>
                                                        <div class="uk-inline right">
                                                            <i class="material-icons">share</i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                            }
                        }
                    }
                }
                ?>
                    </div>

                    <!-- Criar Notas -->

                    <div id="create-notes" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body">
                            <div class="row">
                                <form class="col s12" method="POST" action="./../server/classes/notas/insert.php">
                                    <div class="row">
                                        <div class="col s12">
                                            <div class="uk-margin">
                                                <textarea class="materialize-textarea" rows="5" placeholder="Título da Nota" name="titNota"></textarea>
                                            </div>
                                        </div>
                                        <div class="col s12">
                                            <div class="uk-margin">
                                                <textarea class="uk-textarea" rows="30" placeholder="Escreve o que sentes..." name="detalhesNota" style="height: 150px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="uk-button uk-button-default" name="insertNotesBtn" value="Guardar">
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Criar Etiquetas Modal-->

                    <div id="create_label_modal" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body">
                            <div class="row">
                                <form class="col s12" method="POST" action="./../server/classes/labels/insertlabel.php">
                                    <div class="row">
                                        <div class="col s12">
                                            <div class="input-field">
                                                <textarea class="materialize-textarea" rows="5" placeholder="                           Nome da Etiqueta" name="nomelabel"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="uk-button uk-button-default" name="insertlabelbtn" value="Criar etiqueta">
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Editar Etiqueta Modal -->

                    <div id="edit_label_modal" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body">
                            <div class="row">
                                <form class="col s12" method="POST" action="./../server/classes/labels/editlabel.php">
                                    <div class="row">
                                        <div class="col s12">
                                            <input type="hidden" id="get_id_label" name="id_label">
                                            <div class="input-field">
                                                <textarea class="materialize-textarea" rows="5" placeholder="                           Novo Nome da Etiqueta" name="newnamelabel"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="uk-button uk-button-default" name="editlabelbtn" value="Guardar">
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Associar Etiquetas a Notas -->

                    <div id="assoc_label_to_note" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
                            <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">Associe a sua nota a etiquetas</h4>
                            <div class="row">
                                <div class="col s12 m6">
                                    <form action="./../server/classes/labels/assocnotes.php" method="POST">
                                        <div class="row" id="append_labels_to_assoc"></div>
                                        <input id="id_nota" type="hidden" name="id_nota">
                                        <button class="uk-button uk-button-default" type="submit" name="assocbtn">Guardar alterações</button>
                                    </form>
                                </div>

                                <div class="col s12 m6">
                                    <img src="./../static/imagens/assoclabels.png" style="width: 270px !important;">
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- Share Modal -->

                    <div id="modal-share-card" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
                            <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">Partilhe esta nota com os seus amigos</h4>
                            <div class="row">
                                <div class="col s12 m6">
                                    <div class="row">
                                        <form class="col s12" id="form_share" method="POST" action="./../server/classes/shares/sender.php">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="uk-margin"><br><br>
                                                        <!-- Email de Destino -->
                                                        <i class="material-icons left">email</i>
                                                        <div class="chips" id="emaildestino"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="id_share_modal" name="id_share_modal">
                                            <input type="hidden" id="storage" name="storage">
                                            <input type="submit" class="uk-button uk-button-default" name="sharenotebtn" value="Partilhar">
                                        </form>
                                    </div>
                                </div>
                                <div class="col s12 m6">
                                    <img src="./../static/imagens/share_img.png" style="width: 270px !important;">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
            } else {
                header("Location: http://localhost/myschedule/html/index.php?error=nologin&modal=null");
                exit();
            }
            ?>
            </main>

            <script src="../static/js/app.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.js"></script>

        </body>

        </html>