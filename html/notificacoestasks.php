        <!-- MySchedule - Carlos Ferreira -->
        <?php session_start(); ?>

        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>myschedule</title>
            <link rel="icon" href="../static/imagens/logo_2.png" type="image/ico">

            <link rel="stylesheet" href="../static/css/index.css">
            <link rel="stylesheet" href="../static/css/uikit/uikit.css">
            <link rel="stylesheet" href="../static/css/materialize/css/materialize.css">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
            <script src="../static/js/app.js"></script>
            <script src="../static/js/uikit/uikit.min.js"></script>
            <script src="../static/js/uikit/uikit-icons.min.js"></script>
        </head>

        <body class="white">
            <!-- Barra de Navegação -->
            <div class="navbar-fixed">
                <nav class="white" style="padding: 0 0 75px 0">
                    <div class="container-fluid">
                        <a href="#" data-target="slide-out" class="black-text show-on-medium-and-up sidenav-trigger"><i class="material-icons">menu</i></a>
                        <div class="container">
                            <img width="47" height="47" src="../static/imagens/logo_2.png">
                            <a class="logo" href="http://localhost/myschedule/html/todo.php">myschedule</a>
                            <a class="right hide-on-med-and-down" href="http://localhost/myschedule/html/notificacoes.php">
                                <i class="material-icons black-text" style="line-height: 76px !important; color: #007fff !important">refresh</i>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>

            <ul id="slide-out" class="sidenav"><br>
                <li><a href="http://localhost/MySchedule/html/actions.php"><i class="material-icons">note</i>Notas</a></li>
                <li><a href="http://localhost/MySchedule/html/archive.php"><i class="material-icons">backup</i>Arquivo</a></li>
                <li><a href="http://localhost/MySchedule/html/todo.php"><i class="material-icons">schedule</i>Tarefas</a></li>
                <li><a href="http://localhost/MySchedule/html/notificacoes.php"><i class="material-icons">sms</i>Notificações</a></li>
                <li>
                    <div class="divider"></div>
                </li>
                <li><a onClick="openMyAccount();"><i class="material-icons">person</i>A Minha Conta</a></li>

                <form action="../server/classes/logout.php" method="POST">
                    <input type="submit" class="uk-button uk-button-primary round" style="margin: 65px; margin-bottom: -550px;" name="log-out" value="Sign Out">
                </form>
            </ul>


            <?php
            if (isset($_GET['modal'])) {
                $modal = $_GET['modal'];
                if ($modal == "changedprofile") {
                    echo ("<script>UIkit.modal($('#profileupdated')).show();</script>");
                }
            }

            if (isset($_GET['error'])) {
                echo ("<script>M.toast({html: 'Campos inválidos', classes: 'rounded'});</script>");
            }
            ?>

            <?php
            require '../server/classes/database.php';
            $session_id = $_SESSION['userid'];
            //Receber todos os dados do utilizador
            $sql_get_data_user = "SELECT * FROM users WHERE idUtilizador = ?";
            $stmt_get_data_user = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt_get_data_user, $sql_get_data_user)) {
                header("Location: ../../todo.php");
                exit();
            } else {
                mysqli_stmt_bind_param($stmt_get_data_user, "s", $session_id);
                mysqli_stmt_execute($stmt_get_data_user);

                $result = mysqli_stmt_get_result($stmt_get_data_user);

                if ($row_get_data_user = mysqli_fetch_assoc($result)) {
                    $username = $row_get_data_user['nomeUtilizador'];
                    $email = $row_get_data_user['emailUtilizador'];
                    ?>
                    <input type="hidden" name="nomestorage" id="nomestorage" value="<?php echo $username; ?>">
                    <input type="hidden" name="emailstorage" id="emailstorage" value="<?php echo $email; ?>">

                <?php
            } else {
                echo "<script>console.log('asd');</script>";
            }
        }
        ?>

            <!-- Modal - A minha conta -->
            <div id="aminhaconta" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
                    <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">A Minha Conta</h4>
                    <div class="row">
                        <div class="col s12 m6">
                            <div class="row">
                                <form class="col s12" method="POST" action="./../server/classes/profile/changeprofile.php">
                                    <br>
                                    <div class="row">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="nome" type="text" class="validate" name="nome">
                                    </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s9">
                                    <i class="material-icons prefix">email</i>
                                    <input id="icon_telephone" type="text" class="validate" name="icon_telephone">
                                </div>
                            </div>
                            <input style="margin-left: 298px !important;" type="submit" class="uk-button uk-button-default" name="changeprofile" value="Guardar">
                            </form>
                        </div>
                    </div><br>
                    <div class="col s12 m6">
                        <img src="./../static/imagens/profilepic.png" style="width: 270px !important;">
                    </div>
                </div>
            </div>
            </div>
            <!-- Mostrar Notificações -->

            <?php
            if (isset($_SESSION['userid'])) {
                require('./../server/classes/database.php');

                $session_id = $_SESSION["userid"];

                //Email do user
                $stmt = mysqli_stmt_init($connection);
                $sql = "SELECT emailUtilizador FROM users WHERE idUtilizador = ?";

                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    echo "Erro";
                } else {
                    mysqli_stmt_bind_param($stmt, "i", $session_id);
                    mysqli_stmt_execute($stmt);
                    $result = mysqli_stmt_get_result($stmt);

                    if ($row = mysqli_fetch_assoc($result)) {
                        $email_get_result = $row['emailUtilizador'];
                        $_SESSION['email_user'] = $email_get_result;
                    }
                }

                //Dados da partilha
                $sql_get_values = "SELECT * FROM sharestasks WHERE emailDestino = ?";

                if (!mysqli_stmt_prepare($stmt, $sql_get_values)) {
                    echo "erro";
                } else {
                    mysqli_stmt_bind_param($stmt, "s", $email_get_result);
                    mysqli_stmt_execute($stmt);
                    $result_values = mysqli_stmt_get_result($stmt);

                    //PHP 7.0 - Num_Rows
                    $count_rows = $result_values->num_rows;

                    while ($row_values = $result_values->fetch_array()) {
                        $id_origin = $row_values['idOrigem'];
                        $id_task = $row_values['idTask'];

                        $sql_get_name_origin = "SELECT nomeUtilizador FROM users WHERE idUtilizador = ?";
                        $sql_get_nota = "SELECT nomeTask FROM tasks WHERE idTask = ?";

                        //Query - Nome de Origem (Partilha)
                        if (!mysqli_stmt_prepare($stmt, $sql_get_name_origin)) {
                            echo "Erro";
                        } else {
                            mysqli_stmt_bind_param($stmt, "i", $id_origin);
                            mysqli_stmt_execute($stmt);
                            $result_get_name = mysqli_stmt_get_result($stmt);

                            if ($row_get_name_origin = mysqli_fetch_assoc($result_get_name)) {
                                $name_origin = $row_get_name_origin['nomeUtilizador'];
                            }
                        }

                        //Query - Id Nota (Partilha)
                        if (!mysqli_stmt_prepare($stmt, $sql_get_nota)) {
                            echo "Erro";
                        } else {
                            mysqli_stmt_bind_param($stmt, "i", $id_task);
                            mysqli_stmt_execute($stmt);
                            $result_get_id_nota = mysqli_stmt_get_result($stmt);

                            if ($row_get_id_nota = mysqli_fetch_assoc($result_get_id_nota)) {
                                $nometask = $row_get_id_nota['nomeTask'];
                            }
                        }
                        ?>


                        <div class="container">
                            <div class="row">
                                <div class="col s6 l6 m6" style="margin-top: 30px;">
                                    <div class="uk-card uk-card-default uk-card-body">
                                        <div class="col s6 m6 l6">
                                            <h5><a><i class="material-icons">done_outline</i><?php echo "  " . $name_origin; ?></a></h5><br>
                                            <span><i><?php echo "Tarefa: " . $nometask; ?></i><br>
                                        </div>
                                        <div>
                                            <div class="col s6 m6 l6">
                                                <form action="../server/classes/todo/shares/accepttask.php" method="POST">
                                                    <input type="hidden" name="id_nota" value="<?php echo $id_task ?>">
                                                    <input type="hidden" name="id_origin_input" value="<?php echo $id_origin ?>">
                                                    <input type="hidden" name="email_session" value="<?php echo $email_get_result ?>">
                                                    <input class="waves-effect waves-light btn-small green" type="submit" value="Aceitar" name="save_task_btn" style="border-radius: 20px !important; margin-left: 75px !important;">
                                                </form><br>
                                                <form action="../server/classes/todo/shares/refusetask.php" method="POST">
                                                    <input type="hidden" name="id_nota" value="<?php echo $id_task ?>">
                                                    <input type="hidden" name="id_origin_input" value="<?php echo $id_origin ?>">
                                                    <input type="hidden" name="email_session" value="<?php echo $email_get_result ?>">
                                                    <input class="waves-effect waves-light btn-small red" type="submit" value="Recusar" name="refuse_task_btn" style="border-radius: 20px !important; margin-left: 75px !important; margin-top: 15px !important;">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                }
            }
        } else {
            header("Location: ./../../../../html/actions.php&error=nosession");
        }
        ?>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            <script>
                $(() => {
                    $('.sidenav').sidenav();
                });
            </script>
        </body>

        </html>