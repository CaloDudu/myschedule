    <!-- MySchedule - Carlos Ferreira -->
    <?php session_start(); ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>myschedule</title>
      <link rel="icon" href="../static/imagens/logo_2.png" type="image/ico">

      <link rel="stylesheet" href="../static/css/index.css">
      <link rel="stylesheet" href="../static/css/uikit/uikit.css">
      <link rel="stylesheet" href="../static/css/materialize/css/materialize.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

      <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
      <script src="../static/js/uikit/uikit.min.js"></script>
      <script src="../static/js/uikit/uikit-icons.min.js"></script>
      <script src="../static/js/app.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

      <style>
        header,
        main,
        footer {
          padding-right: 190px;
        }

        a.float {
          right: 270px !important;
        }


        @media only screen and (max-width : 992px) {

          header,
          main,
          footer {
            padding-right: 0;
          }

          a.float {
            bottom: 25px !important;
            right: 10px !important;
          }

        }
      </style>
    </head>

    <body style="background-color: white;">
      <header>
        <ul id="dropdown1" class="dropdown-content">
          <li><a href="../html/todo.php?order=1">Importância</a></li>
          <li><a href="../html/todo.php?order=2">Data limite</a></li>
          <li><a href="../html/todo.php?order=3">Nome</a></li>
        </ul>
        <div class="navbar">
          <nav class="white" style="padding: 0 0 75px 0">
            <div class="nav-wrapper">
              <ul class="container-fluid">
                <li class="left"><a href="#" data-target="slide-out" class="black-text show-on-medium-and-up sidenav-trigger left"><i class="material-icons">menu</i></a></li>
                <li class="left"><img width="47" height="47" src="../static/imagens/logo_2.png"></li>
                <li class="left"><a class="logo blue-text" href="http://localhost/myschedule/html/todo.php">myschedule</a></li>
                <li><a style="margin-left: 540px;" class="dropdown-trigger" href="#!" data-target="dropdown1">Ordem<i class="material-icons right">arrow_drop_down</i></a></li>
              </ul>
            </div>
          </nav>
        </div>

        <ul id="slide-out" class="sidenav"><br>
          <li><a href="http://localhost/myschedule/html/actions.php"><i class="material-icons">note</i>Notas</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a href="http://localhost/myschedule/html/todo.php"><i class="material-icons">schedule</i>Tarefas</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a onClick="openMyAccount();"><i class="material-icons">person</i>A Minha Conta</a></li>

          <form action="../server/classes/index/logout.php" method="POST">
            <input type="submit" class="uk-button uk-button-primary round" style="margin: 65px; margin-bottom: -450px;" name="log-out" value="Sign Out">
          </form>
        </ul>

        <ul id="sidenav_right_side" class="sidenav sidenav-fixed" style="width:230px !important;"><br>
          <li>
            <h6 class="center"><i class="material-icons">more_horiz</i></h6>
          </li>
          <li><a href="http://localhost/MySchedule/html/donetasks.php"><i class="material-icons">done</i>Concluídas</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a href="http://localhost/MySchedule/html/bin.php"><i class="material-icons">delete</i>Lixo</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a href="http://localhost/MySchedule/html/notificacoestasks.php"><i class="material-icons">sms</i>Notificações</a></li>
          <li>
            <div class="divider"></div>
          </li>
        </ul>
      </header>
      <main>

        <!-- Modal - A minha conta -->
        <div id="aminhaconta" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
            <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">A Minha Conta</h4>
            <div class="row">
              <div class="col s12 m6">
                <div class="row">
                  <form class="col s12" method="POST" action="./../server/classes/profile/changeprofile.php">
                    <br>
                    <div class="row">
                      <div class="input-field col s9">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="nome" type="text" class="validate" name="nome">
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s9">
                        <i class="material-icons prefix">email</i>
                        <input id="icon_telephone" type="text" class="validate" name="icon_telephone">
                      </div>
                    </div>
                    <input style="margin-left: 298px !important;" type="submit" class="uk-button uk-button-default" name="changeprofile" value="Guardar">
                  </form>
                </div>
              </div><br>
              <div class="col s12 m6">
                <img src="./../static/imagens/profilepic.png" style="width: 270px !important;">
              </div>
            </div>
          </div>
        </div>

        <!-- Modal - Nota terminada com sucesso -->
        <div id="taskfinished" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="border-radius: 10px">
            <h6 style="font-weight: bold;">Tarefa concluída com sucesso !</h6>
            <br>
            <img width="300" height="300" src="../static/imagens/finish.png">
          </div>
        </div>

        <!-- Modal - Perfil alterado com sucesso -->
        <div id="profileupdated" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="border-radius: 10px">
            <h6 style="font-weight: bold;">Perfil alterado com sucesso !</h6>
            <br>
            <img width="300" height="300" src="../static/imagens/celebration.png">
          </div>
        </div>

        <?php
        if (isset($_GET['modal'])) {
          $modal = $_GET['modal'];
          if ($modal == "taskfinished") {
            echo ("<script>UIkit.modal($('#taskfinished')).show();</script>");
          } else if ($modal == "changedprofile") {
            echo ("<script>UIkit.modal($('#profileupdated')).show();</script>");
          }
        }

        if (isset($_GET['error'])) {
          echo ("<script>M.toast({html: 'Campos inválidos', classes: 'rounded'});</script>");
        }
        ?>

        <!-- Nota Partilhada Modal -->
        <div id="sent_shared_note" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="border-radius: 10px">
            <h6 style="font-weight: bold;">A sua tarefa foi partilhada. Seja produtivo!</h6>
            <br>
            <img width="300" height="300" src="../static/imagens/transfer_files.png">
          </div>
        </div>

        <?php
        if (isset($_GET['modal'])) {
          $modal = $_GET['modal'];
          if ($modal == "sent_task_sucess") {
            echo ("<script>UIkit.modal($('#sent_shared_note')).show();</script>");
          }
        }
        ?>

        <?php
        require '../server/classes/database.php';
        $session_id = $_SESSION['userid'];
        //Receber todos os dados do utilizador
        $sql_get_data_user = "SELECT * FROM users WHERE idUtilizador = ?";
        $stmt_get_data_user = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt_get_data_user, $sql_get_data_user)) {
          header("Location: ../../todo.php");
          exit();
        } else {
          mysqli_stmt_bind_param($stmt_get_data_user, "s", $session_id);
          mysqli_stmt_execute($stmt_get_data_user);

          $result = mysqli_stmt_get_result($stmt_get_data_user);

          if ($row_get_data_user = mysqli_fetch_assoc($result)) {
            $username = $row_get_data_user['nomeUtilizador'];
            $email = $row_get_data_user['emailUtilizador'];
            ?>
            <input type="hidden" name="nomestorage" id="nomestorage" value="<?php echo $username; ?>">
            <input type="hidden" name="emailstorage" id="emailstorage" value="<?php echo $email; ?>">

          <?php
        } else {
          echo "<script>console.log('asd');</script>";
        }
      }
      ?>

        <!-- Modal - Create Task -->
        <div id="create-tasks" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
            <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">Personalize a sua tarefa</h4>
            <div class="row">
              <div class="col s12 m6">
                <div class="row">
                  <form id="form_share_task" method="POST" action="./../server/classes/todo/insert.php">
                    <div class="row">
                      <div class="col s12">
                        <div class="uk-margin">
                          <div class="input-field col s6" style="width: 350px !important;">
                            <i class="material-icons prefix">edit</i>
                            <input id="icon_prefix" type="text" class="validate" name="nameEtiquetaValue">
                            <label for="icon_prefix">Nome da Tarefa</label>
                          </div>
                        </div>
                      </div>
                      <div class="col s12">
                        <div class="input-field col s12" style="margin-left: 44px !important; width: 310px !important;">
                          <select name="selectPriority">
                            <option value="1">Pouco Importante</option> <!-- Verde -->
                            <option value="2">OK. Faço Amanhã</option> <!-- Amarelo -->
                            <option value="3">Importantíssimo</option> <!-- Vermelho -->
                          </select>
                          <label>Importância da tarefa</label>
                        </div>
                      </div>
                      <input name="dataTask" style="margin-left: 24px !important; width: 290px !important;" type="text" class="datepicker" placeholder="Data Limite da Tarefa" style="margin-left: 10px !important;">
                    </div>
                    <input type="submit" class="uk-button uk-button-default" name="insertTasksButton" value="Guardar">
                  </form>
                </div>
              </div>
              <div class="col s12 m6">
                <img src="./../static/imagens/create_task_modal_img.png" style="width: 270px !important;">
              </div>
            </div>
          </div>
        </div>

        <!-- Share Modal - Tasks -->

        <div id="modal-share-task-card" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
            <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">Seja produtivo com os seus amigos</h4>
            <div class="row">
              <div class="col s12 m6">
                <div class="row">
                  <form class="col s12" id="form_share" method="POST" action="./../server/classes/todo/shares/sender_tasks.php">
                    <div class="row">
                      <div class="col s12">
                        <div class="uk-margin"><br><br>
                          <!-- Email de Destino -->
                          <i class="material-icons left">email</i>
                          <div class="chips" id="emaildestino"></div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" id="id_share_task" name="id_share_task">
                    <input type="hidden" id="storage" name="storage">
                    <input type="submit" class="uk-button uk-button-default" name="sharetaskbtn" value="Partilhar">
                  </form>
                </div>
              </div>
              <div class="col s12 m6">
                <img src="./../static/imagens/sharetaskimage.png" style="width: 270px !important;">
              </div>
            </div>
          </div>
        </div>

        <!-- Modal - Tarefas -->
        <div id="modal-card-task" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="width:850px !important;">
            <div class="row">
              <div class="col m6 s6 l6">
                <div class="row">
                  <form class="col s12" method="POST" action="./../server/classes/todo/update.php" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col s12">
                        <div class="uk-margin">
                          <textarea class="uk-textarea" id="name_task_write" name="name_task_write" style="height: 40px; font-weight: bold; font-size: 18px;"></textarea>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" id="id_anexar" name="id_anexar">
                    <input type="hidden" id="id1" name="id">
                    <span id="datetime_compare"></span>
                    <div class="input-field col s12">
                      <select name="selectPriorityUpdate" id="changePriority">
                        <option value="1">Pouco Importante</option>
                        <option value="2">OK. Faço Amanhã</option>
                        <option value="3">Importantíssimo</option>
                      </select>
                      <label>Importância da tarefa</label>
                    </div>
                    <br><br>
                    <input id="insertdatetime_task" name="dataTaskEditar" type="text" class="datepicker" style="margin-left: 10px !important;">
                </div>
              </div>
              <div class="row">
                <img src="../static/imagens/edittask.png" width="250px"><br><br>
                <input type="submit" class="uk-button uk-button-default" name="inserTaskButton" value="Guardar">
              </div>
              </form>
            </div>
          </div>
        </div>

        <br><br>
        <!-- Mostrar Tarefas -->
        <div class="container">
          <div class="timeline">
            <?php
            require '../server/classes/database.php';

            $iduser = $_SESSION['userid'];
            $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 0 OR status = 5 ORDER by status DESC";

            if (isset($_GET['order'])) {
              $value = $_GET['order'];
              // 1 - Importância
              // 2 - Data Limite
              // 3 - Nome
              if ($value == 1) {
                $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 0 OR status = 5 ORDER BY importanciaTask DESC";
              } else if ($value == 2) {
                $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 0 OR status = 5 ORDER BY dataLimiteTask";
              } else {
                $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 0 OR status = 5 ORDER BY nomeTask";
              }
            }

            $stmt_select_all_tasks = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt_select_all_tasks, $sql_select_all_tasks)) {
              exit();
            } else {
              mysqli_stmt_bind_param($stmt_select_all_tasks, "s", $iduser);
              mysqli_stmt_execute($stmt_select_all_tasks);

              $result_query_select_all_tasks = mysqli_stmt_get_result($stmt_select_all_tasks);

              $cor = "azul";
              while ($row = $result_query_select_all_tasks->fetch_array()) {
                $nome = $row['nomeTask'];
                $prioridade = $row['importanciaTask'];
                $datalimite = $row['dataLimiteTask'];
                $id_task = $row['idTask'];
                $status = $row['status'];

                if ($prioridade == 1) {
                  $cor = "blue";
                } else if ($prioridade == 2) {
                  $cor = "yellow";
                } else {
                  $cor = "red";
                }

                $cor_completo = "timeline-badge white-text " . $cor;
                ?>

                <div class="timeline-event">
                  <div class="card timeline-content">
                    <form action="" method="POST">
                      <div class="card-content" valormodal="valormodal" id="<?php echo $id_task ?>">
                        <span id="id_task" style="display: none;"><?php echo $id_task; ?></span>
                        <p id="name_task" style="display:none"><?php echo $nome; ?></p>
                        <p id="priority_task_to_write" style="display:none"><?php echo $prioridade ?></p>
                        <input id="datetime_write_task" type="hidden" value="<?php echo $datalimite; ?>">

                        <div class="uk-inline right">
                          <?php
                          if ($status == 5) {
                            echo "<i class='material-icons right' style='line-height: 23px !important;'>flag</i>"; ?>
                            <i class="material-icons right" style="line-height: 23px !important;">more_vert</i>
                            <div uk-dropdown="pos: right-left; duration: 100">
                              <ul class="uk-nav uk-dropdown-nav">
                                <li id="<?php echo ($id_task); ?>" onClick="openModalEditTask(this,this.id);"><a href="#"> <i class="material-icons" style="vertical-align: sub !important;">edit</i> Editar</a></li>
                                <li> <a href="../server/classes/todo/retiraroafixo.php?id=<?php echo $id_task; ?>"> <i class="material-icons" style="vertical-align: sub !important;">expand_less</i> Desafixar</a></li>
                                <li> <a href="../server/classes/todo/delete.php?id=<?php echo ($id_task); ?>"><i class="material-icons" style="vertical-align: sub !important;">delete_outline</i> Eliminar</a></li>
                                <li id="<?php echo ($id_task); ?>" onClick="openModalShareTask(this.id);"><a href="#"> <i class="material-icons" style="vertical-align: sub !important;">import_export</i> Partilhar</a></li>
                            </div>
                          <?php
                        } else { ?>
                            <i class="material-icons right" style="line-height: 23px !important;">more_vert</i>
                            <div uk-dropdown="pos: right-left; duration: 100">
                              <ul class="uk-nav uk-dropdown-nav">
                                <li id="<?php echo ($id_task); ?>" onClick="openModalEditTask(this,this.id);"><a href="#"> <i class="material-icons" style="vertical-align: sub !important;">edit</i> Editar</a></li>
                                <li> <a href="../server/classes/todo/afixar.php?id=<?php echo $id_task; ?>"> <i class="material-icons" style="vertical-align: sub !important;">expand_less</i> Afixar</a></li>
                                <li> <a href="../server/classes/todo/delete.php?id=<?php echo ($id_task); ?>"><i class="material-icons" style="vertical-align: sub !important;">delete_outline</i> Eliminar</a></li>
                                <li id="<?php echo ($id_task); ?>" onClick="openModalShareTask(this.id);"><a href="#"> <i class="material-icons" style="vertical-align: sub !important;">import_export</i> Partilhar</a></li>
                            </div>
                          <?php
                        }
                        ?>

                        </div>
                        <?php echo "<font size=5>" . $nome . "</font><br><font size=2>(" . $datalimite . ")</font>" ?>
                        <p class="right">
                          <a id="<?php echo $id_task ?>" class="tooltipped" data-position="bottom" data-tooltip="Já está!" href="../server/classes/todo/changestatus.php?id=<?php echo $id_task ?>"><i class="material-icons">check_circle_outline</i></i></a>
                        </p>
                      </div>
                    </form>
                  </div>
                  <?php
                  $datatime = date('Y-m-d');
                  if ($datalimite > $datatime) { ?>
                    <div class="<?php echo $cor_completo; ?>"><a href="#"><i class="material-icons white-text">face</i></i></a></div>
                  <?php
                } else { ?>
                    <div class="<?php echo $cor_completo; ?>"><a href="#"><i class="material-icons white-text">report_problem</i></i></a></div>
                  <?php
                }

                ?>
                </div>
              <?php
            }
            //Receber dados de tarefas partilhadas

            $session_id = $_SESSION['userid'];
            $sql_get_email_from_this_user = "SELECT emailUtilizador FROM users WHERE idUtilizador = ?";
            $stmt_get_email_from_this_user = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt_get_email_from_this_user, $sql_get_email_from_this_user)) {
              echo "Erro";
            } else {
              mysqli_stmt_bind_param($stmt_get_email_from_this_user, "i", $session_id);
              mysqli_stmt_execute($stmt_get_email_from_this_user);
              $result_get_email_from_this_user = mysqli_stmt_get_result($stmt_get_email_from_this_user);

              if ($row_get_email_from_this_user = mysqli_fetch_assoc($result_get_email_from_this_user)) {
                $email_utilizador = $row_get_email_from_this_user['emailUtilizador'];

                $sql_get_storage_with_this_email = "SELECT * FROM sharestasks WHERE emailDestino = ? AND resultado_trans = 1";
                $stmt_get_storage_with_this_email = mysqli_stmt_init($connection);

                if (!mysqli_stmt_prepare($stmt_get_storage_with_this_email, $sql_get_storage_with_this_email)) {
                  echo "Erro";
                } else {
                  mysqli_stmt_bind_param($stmt_get_storage_with_this_email, "s", $email_utilizador);
                  mysqli_stmt_execute($stmt_get_storage_with_this_email);
                  $result_get_storage_with_this_email = mysqli_stmt_get_result($stmt_get_storage_with_this_email);

                  while ($row_get_storage_with_this_email = $result_get_storage_with_this_email->fetch_array()) {
                    $idtask = $row_get_storage_with_this_email['idTask'];
                    $emaildestino = $row_get_storage_with_this_email['emailDestino'];

                    if ($email_utilizador == $emaildestino) {
                      //Verificação - Email de bd corresponde ao email do utilizador corrente
                      $sql_data = "SELECT * FROM tasks WHERE idTask = ? AND status = 0";
                      $stmt_data = mysqli_stmt_init($connection);
                      //Recolher dados da task com determinado id ~(var id_task)

                      if (!mysqli_stmt_prepare($stmt_data, $sql_data)) { } else {
                        mysqli_stmt_bind_param($stmt_data, "i", $idtask);
                        mysqli_stmt_execute($stmt_data);
                        $result_data = mysqli_stmt_get_result($stmt_data);

                        if ($row_data = mysqli_fetch_assoc($result_data)) {
                          $nometask_s = $row_data['nomeTask'];
                          $datalimite_s = $row_data['dataLimiteTask'];
                          $importancia_s = $row_data['importanciaTask'];
                          $status_s = $row_data['status']; ?>

                            <div class="timeline-event">
                              <div class="card timeline-content">
                                <form action="" method="POST">
                                  <div class="card-content" valormodal="valormodal" id="<?php echo $idtask ?>">
                                    <span id="id_task" style="display: none;"><?php echo $idtask; ?></span>
                                    <p id="name_task" style="display:none"><?php echo $nometask_s; ?></p>
                                    <p id="priority_task_to_write" style="display:none"><?php echo $importancia_s ?></p>
                                    <input id="datetime_write_task_s" type="hidden" value="<?php echo $datalimite_s; ?>">

                                    <div class="uk-inline right">
                                      <?php
                                      echo "<i class='material-icons right' style='line-height: 23px !important;'>share</i>"; ?>
                                      <i class="material-icons right" style="line-height: 23px !important;">more_vert</i>
                                      <div uk-dropdown="pos: right-left; duration: 100">
                                        <ul class="uk-nav uk-dropdown-nav">
                                          <li> <a href="../server/classes/todo/delete.php?id=<?php echo ($idtask); ?>"><i class="material-icons" style="vertical-align: sub !important;">delete_outline</i> Eliminar</a></li>
                                      </div>
                                      <?php
                                      ?>

                                    </div>
                                    <?php echo "<font size=5>" . $nometask_s . "</font><br><font size=2>(" . $datalimite_s . ")</font>" ?>
                                    <p class="right">
                                      <a class="tooltipped" data-position="bottom" data-tooltip="Já está!" id="<?php echo $idtask ?>" href="../server/classes/todo/changestatus.php?id=<?php echo $idtask ?>"><i class="material-icons">check_circle_outline</i></i></a>
                                    </p>
                                  </div>
                                </form>
                              </div>
                              <?php
                              $datatime = date('Y-m-d');

                              if ($importancia_s == 1) {
                                $cor = "blue";
                              } else if ($importancia_s == 2) {
                                $cor = "yellow";
                              } else {
                                $cor = "red";
                              }

                              $cor_completo = "timeline-badge white-text " . $cor;

                              if ($datalimite_s > $datatime) { ?>
                                <div class="<?php echo $cor_completo; ?>"><a href="#"><i class="material-icons white-text">face</i></i></a></div>
                              <?php
                            } else { ?>
                                <div class="<?php echo $cor_completo; ?>"><a href="#"><i class="material-icons white-text">report_problem</i></i></a></div>
                              <?php
                            }

                            ?>
                            </div>

                          <?php }
                      }
                    } else { }
                  }
                }
              }
            }
          }
          ?>
          </div>
        </div>

        <div class="floating-btn">
          <a id="meugrandebutton" href="#" class="float tooltipped" data-position="top" data-tooltip="Cria tarefas!" uk-toggle="target: #create-tasks"><i class="fa fa-plus my-float"></i></a>
        </div>
      </main>
      <script src="../static/js/app.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.js"></script>

    </body>

    </html>