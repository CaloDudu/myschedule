        <!-- MySchedule - Carlos Ferreira -->
        <?php session_start(); ?>

        <!DOCTYPE html>
        <html lang="en">

        <head>

            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>myschedule</title>
            <link rel="icon" href="../static/imagens/logo_2.png" type="image/ico">

            <link rel="stylesheet" href="../static/css/index.css">
            <link rel="stylesheet" href="../static/css/uikit/uikit.css">
            <link rel="stylesheet" href="../static/css/materialize/css/materialize.css">

            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
            <script src="../static/js/uikit/uikit.min.js"></script>
            <script src="../static/js/uikit/uikit-icons.min.js"></script>
            <script src="../static/js/app.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        </head>

        <body class="white">

            <!-- Barra de Navegação -->
            <div class="navbar-fixed">
                <nav class="white" style="padding: 0 0 75px 0">
                    <div class="container">
                        <img width="47" height="47" src="../static/imagens/logo_2.png">
                        <a class="logo" href="http://localhost/myschedule/html/index.php">myschedule</a>
                        <ul id="nav-mobile" class="right hide-on-med-and-down">
                            <button uk-toggle="target: #my-id" type="button" class="uk-button uk-button-primary round">Login</button>
                            <button uk-toggle="target: #sentemail" class="hide">
                                <button uk-toggle="target: #reset-pwd-modal" class="hide">
                        </ul>
                    </div>
                </nav>
            </div>

            <!-- Reset Modal -->
            <div id="reset-modal" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" style="border-radius: 15px">
                    <form action="../server/classes/index/forgottenpassword.php" method="POST">
                        <h5>Recupere a sua password.</h5><br>
                        <div class="container-fluid row">
                            <div class="col s12 m12 l12">
                                <input onfocus="this.placeholder=''" onblur="this.placeholder='Email'" maxlength="28" class="login-text-input" name="email-reset" placeholder="Email" type="text"><br><br>
                            </div>
                        </div>

                        <input type="submit" class="uk-button uk-button-default" name="reset-password-submit" value="Enviar">
                    </form>
                </div>
            </div>


            <!-- Get Email Modal -->
            <div id="get_email_modal" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" style="border-radius: 15px">
                    <form action="../server/classes/index/forgottenemail.php" method="POST">
                        <h5>Recupere a sua conta.</h5><br>
                        <div class="container-fluid row">
                            <div class="col s12 m12 l12">
                                <input onfocus="this.placeholder=''" onblur="this.placeholder='Email'" maxlength="28" class="login-text-input" name="getemail" placeholder="Email" type="text"><br><br>
                            </div>
                        </div>

                        <input type="submit" class="uk-button uk-button-default" name="get-email-submit" value="Enviar">
                    </form>
                </div>
            </div>

            <!-- Sign up Sucesso Modal -->
            <div id="modal_sucesso" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" style="border-radius: 10px">
                    <h6 style="font-weight: bold;">Conta criada com sucesso!</h6>
                    <br>
                    <img width="300" height="300" src="../static/imagens/contacriada.png">
                </div>
            </div>


            <?php
            if (isset($_GET["error"]) or isset($_GET["modal"])) {
                if (isset($_GET["error"])) {
                    if (isset($_GET["modal"])) {
                        $modal = $_GET["modal"];
                        if ($modal == "reset-modal") {
                            $error = $_GET["error"];

                            echo ("<script>UIkit.modal($('#reset-modal')).show();</script>");

                            if ($error = "badrequest") {
                                echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                            }
                        }
                    }
                } else {
                    $modal = $_GET["modal"];
                    if ($modal == "reset-modal") {
                        echo ("<script>UIkit.modal($('#reset-modal')).show();</script>");
                    }
                }
            }
            if (isset($_GET['signup'])) {
                $bool = $_GET['signup'];
                if ($bool == "sucess") {
                    echo ("<script>UIkit.modal($('#modal_sucesso')).show();</script>");
                }
            }
            ?>


            <!-- Sent Email Modal -->
            <div id="sentemail" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" style="border-radius: 10px">
                    <h6 style="font-weight: bold;">Verifique o seu email.</h6>
                    <br>
                    <img width="300" height="300" src="../static/imagens/mailsent.png">
                </div>
            </div>



            <!-- Change Password Modal -->
            <div id="reset-pwd-modal" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" style="border-radius: 10px">
                    <h6 style="font-weight: bold;">Modifique a sua password.</h6>
                    <?php
                    if (isset($_GET["modal"])) {
                        $modal = $_GET["modal"];
                        if ($modal == "reset-password-modal") {
                            if (isset($_GET["error"])) {
                                $error = $_GET["error"];

                                if ($error == "pwddif") {
                                    echo ("<script>M.toast({html: 'Passwords desiguais', classes: 'rounded'});</script>");
                                    echo ("<script>UIkit.modal($('#reset-pwd-modal')).show();</script>");
                                } else if ($error == "emptyfields") {
                                    echo ("<script>M.toast({html: 'Preencha os campos', classes: 'rounded'});</script>");
                                    echo ("<script>UIkit.modal($('#reset-pwd-modal')).show();</script>");
                                }
                            }
                        }
                        ?><form action="../server/classes/index/resetpasswordaction.php" method="POST">

                            <?php

                            if (isset($_GET["selector"]) and isset($_GET["validator"])) {
                                $selector = $_GET["selector"];
                                $validator = $_GET["validator"];
                            }

                            ?>

                            <input type="hidden" name="selector" value="<?php echo $selector ?>">
                            <input type="hidden" name="validator" value="<?php echo $validator ?>">


                            <div class="container-fluid row">
                                <div class="input-field col s12 m12 l12">
                                    <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Password'" name="password" type="password" placeholder="Password">
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Repita a Password'" name="password-repeat" type="password" placeholder="Repita a Password">
                                </div>
                            </div>
                            <button type="submit" name="reset-button-submit" class="uk-button uk-button-default">Mudar password</button>
                        </form> <?php
                        } else {
                            if (isset($_GET["selector"]) and isset($_GET["validator"])) {
                                $selector = $_GET["selector"];
                                $validator = $_GET["validator"];

                                if (empty($selector) || empty($validator)) {
                                    echo "Impossível validar o request";
                                } else {
                                    if (ctype_xdigit($selector) !== false || ctype_xdigit($validator) !== false) {
                                        ?>
                                    <form action="../server/classes/index/resetpasswordaction.php" method="POST">

                                        <input type="hidden" name="selector" value="<?php echo $selector ?>">
                                        <input type="hidden" name="validator" value="<?php echo $validator ?>">


                                        <div class="container-fluid row">
                                            <div class="input-field col s12 m12 l12">
                                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Password'" name="password" type="password" placeholder="Password">
                                            </div>
                                            <div class="input-field col s12 m12 l12">
                                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Repita a Password'" name="password-repeat" type="password" placeholder="Repita a Password">
                                            </div>
                                        </div>
                                        <button type="submit" name="reset-button-submit" class="uk-button uk-button-default">Mudar password</button>
                                    </form>
                                <?php
                            }
                        }
                    }
                }
                ?>
                </div>
            </div>

            <!-- Login Modal -->
            <div id="my-id" uk-modal>
                <div class="uk-modal-dialog uk-modal-body" style="border-radius: 15px;">
                    <form action="../server/classes/index/login.php" method="POST">
                        <h5 class="uk-modal-title" style="font-weight:bold;">Login</h5>

                        <div class="container-fluid row">
                            <div class="input-field col s12 m12 l12">
                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Username / Email'" name="usernameInput" type="text" placeholder="Username / Email">
                            </div>
                            <div class="input-field col s12 m12 l12">
                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Password'" name="passwordInput" type="password" placeholder="Password" id="password">
                            </div>
                        </div>
                        <input type="submit" class="uk-button uk-button-default" name="login-submit" value="Sign In"> <br><br>

                        <button uk-toggle="target: #reset-modal" type="submit" class="reset-button">Esqueceu-se da password?</button><br><br>
                        <button uk-toggle="target: #get_email_modal" type="submit" class="reset-button">Esqueceu-se da sua conta?</button>
                    </form>
                </div>
            </div>

            <!-- Register Modal -->
            <div id="comeca-ja" uk-modal>
                <div class="uk-modal-dialog uk-modal-body register" style="border-radius: 10px">
                    <form action="../server/classes/index/register.php" method="POST">

                        <h6 class="uk-modal-title" style="font-weight:bold;">Regista-te agora, é grátis!</h6><br>

                        <div class="container-fluid row">
                            <div class="col s12 m6 l6">
                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Nome de Utilizador'" name="usernameInputReg" type="text" placeholder="Nome de Utilizador">
                            </div>

                            <div class="col s12 m6 l6">
                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" name="emailInputReg" type="text" placeholder="Email">
                            </div>
                        </div>

                        <div class="container-fluid row">
                            <div class="col s12 m6 l6">
                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Password'" name="passwordInputReg" type="password" placeholder="Password">
                            </div>

                            <div class="col s12 m6 l6">
                                <input maxlength="28" class="login-text-input" onfocus="this.placeholder=''" onblur="this.placeholder='Repita a Password'" name="passwordInputReg-repeat" type="password" placeholder="Repita a Password">
                            </div>
                        </div>

                        <input type="submit" class="uk-button uk-button-default" name="reg-submit" value="Criar conta">
                    </form>
                </div>
            </div>

            <!-- Erros -->
            <?php
            if (isset($_GET["error"]) or isset($_GET["modal"])) {
                if (!isset($_GET["error"])) {
                    $modal = $_GET["modal"];
                    if ($modal == "myid") {
                        echo ("<script>UIkit.modal($('#my-id')).show();</script>");
                    } else if ($modal == "register") {
                        if (isset($_GET["uid"]) or isset($_GET["mail"])) {
                            $username = $_GET["uid"];
                            $email = $_GET["mail"];

                            if (empty($username) || empty($email)) {

                                echo ("<script>const y = UIkit.modal($('#comeca-ja')).show();</script>");

                                if (empty($username) and !empty($email)) {
                                    echo ("<script>document.getElementsByName('emailInputReg')[0].placeholder='" . $email . "'</script>");

                                    if ($error == "emptyfields") {
                                        echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                    } else if ($error == "wrongpassword") {
                                        echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                    } else if ($error == "nouser") {
                                        echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                    } else if ($error == "sqlerror") {
                                        echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                    } else if ($error == "invalidemail") {
                                        echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                    } else if ($error == "useroremailtaken") {
                                        echo ("<script>M.toast({html: 'Esse email ou username já está a ser utilizado', classes: 'rounded'});</script>");
                                    } else if ($error == "passwordcheck") {
                                        echo ("<script>M.toast({html: 'Passwords desiguais', classes: 'rounded'});</script>");
                                    }
                                } else if (empty($email) and !empty($username)) {
                                    echo ("<script>document.getElementsByName('usernameInputReg')[0].placeholder='" . $username . "'</script>");
                                    if ($error == "emptyfields") {
                                        echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                    } else if ($error == "wrongpassword") {
                                        echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                    } else if ($error == "nouser") {
                                        echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                    } else if ($error == "sqlerror") {
                                        echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                    } else if ($error == "invalidemail") {
                                        echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                    }
                                } else {
                                    $error = $_GET["error"];

                                    echo ("<script>const y = UIkit.modal($('#comeca-ja')).show();</script>");

                                    if ($error == "emptyfields") {
                                        echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                    } else if ($error == "wrongpassword") {
                                        echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                    } else if ($error == "nouser") {
                                        echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                    } else if ($error == "sqlerror") {
                                        echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                    } else if ($error == "invalidemail") {
                                        echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                    }
                                }
                            } else {
                                $error = $_GET["error"];

                                echo ("<script>const y = UIkit.modal($('#comeca-ja')).show();</script>");

                                if ($error == "emptyfields") {
                                    echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                } else if ($error == "wrongpassword") {
                                    echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                } else if ($error == "nouser") {
                                    echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                } else if ($error == "sqlerror") {
                                    echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                } else if ($error == "invalidemail") {
                                    echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                }
                            }
                        }
                    } else if ($modal == "emailsent") {
                        echo ("<script>UIkit.modal($('#sentemail')).show();</script>");
                    } else if ($modal == "reset-password-modal") {
                        echo ("<script>UIkit.modal($('#reset-pwd-modal')).show();</script>");
                    }
                } else {
                    if (isset($_GET["error"]) or isset($_GET["modal"])) {
                        if (isset($_GET["modal"])) {
                            $modal = $_GET["modal"];
                            $error = $_GET["error"];
                            if ($modal == "myid") {
                                echo ("<script>UIkit.modal($('#my-id')).show();</script>");

                                if ($error == "emptyfields") {
                                    echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                } else if ($error == "wrongpassword") {
                                    echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                } else if ($error == "nouser") {
                                    echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                } else if ($error == "sqlerror") {
                                    echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                } else if ($error == "invalidemail") {
                                    echo ("<script>M.toast({html: 'Email inválido.', classes: 'rounded'});</script>");
                                } else if ($error == "useroremailtaken") {
                                    echo ("<script>M.toast({html: 'Esse email ou username já está a ser utilizado', classes: 'rounded'});</script>");
                                } else if ($error == "passwordcheck") {
                                    echo ("<script>M.toast({html: 'Passwords desiguais', classes: 'rounded'});</script>");
                                }
                            } else if ($modal == "register") {
                                if (isset($_GET["uid"]) or isset($_GET["mail"])) {

                                    $username = $_GET["uid"];
                                    $email = $_GET["mail"];

                                    if (empty($username) || empty($email)) {

                                        echo ("<script>const y = UIkit.modal($('#comeca-ja')).show();</script>");

                                        if (empty($username) and !empty($email)) {
                                            echo ("<script>document.getElementsByName('emailInputReg')[0].placeholder='" . $email . "'</script>");

                                            if ($error == "emptyfields") {
                                                echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                            } else if ($error == "wrongpassword") {
                                                echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                            } else if ($error == "nouser") {
                                                echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                            } else if ($error == "sqlerror") {
                                                echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                            } else if ($error == "invalidemail") {
                                                echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                            } else if ($error == "useroremailtaken") {
                                                echo ("<script>M.toast({html: 'Esse email ou username já está a ser utilizado', classes: 'rounded'});</script>");
                                            } else if ($error == "passwordcheck") {
                                                echo ("<script>M.toast({html: 'Passwords desiguais', classes: 'rounded'});</script>");
                                            }
                                        } else if (empty($email) and !empty($username)) {
                                            echo ("<script>document.getElementsByName('usernameInputReg')[0].placeholder='" . $username . "'</script>");
                                            if ($error == "emptyfields") {
                                                echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                            } else if ($error == "wrongpassword") {
                                                echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                            } else if ($error == "nouser") {
                                                echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                            } else if ($error == "sqlerror") {
                                                echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                            } else if ($error == "invalidemail") {
                                                echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                            } else if ($error == "useroremailtaken") {
                                                echo ("<script>M.toast({html: 'Esse email ou username já está a ser utilizado', classes: 'rounded'});</script>");
                                            } else if ($error == "passwordcheck") {
                                                echo ("<script>M.toast({html: 'Passwords desiguais', classes: 'rounded'});</script>");
                                            }
                                        } else {
                                            $error = $_GET["error"];

                                            echo ("<script>const y = UIkit.modal($('#comeca-ja')).show();</script>");

                                            if ($error == "emptyfields") {
                                                echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                            } else if ($error == "wrongpassword") {
                                                echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                            } else if ($error == "nouser") {
                                                echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                            } else if ($error == "sqlerror") {
                                                echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                            } else if ($error == "invalidemail") {
                                                echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                            } else if ($error == "useroremailtaken") {
                                                echo ("<script>M.toast({html: 'Esse email ou username já está a ser utilizado', classes: 'rounded'});</script>");
                                            } else if ($error == "passwordcheck") {
                                                echo ("<script>M.toast({html: 'Passwords desiguais', classes: 'rounded'});</script>");
                                            }
                                        }
                                    } else {
                                        $error = $_GET["error"];

                                        echo ("<script>const y = UIkit.modal($('#comeca-ja')).show();</script>");

                                        if ($error == "emptyfields") {
                                            echo ("<script>M.toast({html: 'Preencha todos os campos', classes: 'rounded'});</script>");
                                        } else if ($error == "wrongpassword") {
                                            echo ("<script>M.toast({html: 'Password errada', classes: 'rounded'});</script>");
                                        } else if ($error == "nouser") {
                                            echo ("<script>M.toast({html: 'Utilizador inexistente', classes: 'rounded'});</script>");
                                        } else if ($error == "sqlerror") {
                                            echo ("<script>M.toast({html: 'Tente outra vez.', classes: 'rounded'});</script>");
                                        } else if ($error == "invalidemail") {
                                            echo ("<script>M.toast({html: 'Email inválido', classes: 'rounded'});</script>");
                                        } else if ($error == "useroremailtaken") {
                                            echo ("<script>M.toast({html: 'Esse email ou username já está a ser utilizado', classes: 'rounded'});</script>");
                                        } else if ($error == "passwordcheck") {
                                            echo ("<script>M.toast({html: 'Passwords desiguais', classes: 'rounded'});</script>");
                                        }
                                    }
                                }
                            } else if ($modal == "emailsent") {
                                echo ("<script>UIkit.modal($('#sentemail')).show();</script>");
                            } else if ($modal == "reset-password-modal") {
                                echo ("<script>UIkit.modal($('#reset-pwd-modal')).show();</script>");
                            }
                        }
                    }
                }
            }

            ?>

            <!-- Primeira Row -->
            <section class="greydiv">
                <div class="container row">
                    <div class="col s12 m6 l6"><br><br><br><br>
                        <h1>Simplifica a tua vida.</h1>
                        <h5>MySchedule organiza e simplifica a vida de milhões diariamente. Organiza tudo quando quiseres e onde quiseres.</h5>
                        <br><button uk-toggle="target: #comeca-ja" type="button" class="uk-button uk-button-primary round">Começa ja!</button>
                    </div>

                    <div class="col s12 m6 l6"><br><br><br><br><br><br>
                        <img width="400" height="400" src="../static/imagens/imagem1.png">
                    </div>
                </div>
                <br><br><br><br><br>
            </section>

            <!-- Segunda Row -->
            <section>
                <div class="container row">
                    <div class="col s12 m6 l6"><br><br><br><br><br><br><br>
                        <img width="300" height="300" src="../static/imagens/imagem2.png">
                    </div>

                    <div class="col s12 m6 l6"><br><br><br><br><br>
                        <h6 class="h52row">Rápido.</h6>
                        <h4 class="h42row">Organiza as tuas tarefas, lembretes e notas.</h4>
                        <h5>Mantém os teus objetivos em mente. Adiciona lembretes para que nunca te esqueças de nada.</h5>
                    </div>
                </div>
                <br><br><br><br><br><br>
            </section>

            <!-- Terceira Row -->
            <section class="greydiv">
                <div class="container row">
                    <div class="col s12 m6 l6"><br><br><br><br>
                        <h6 class="h52row">Eficaz.</h6>
                        <h4 class="h43row">Sê produtivo. Partilha com os teus amigos. </h4>
                        <h5>Não desistas. Nunca desistas.</h5>
                        <br><button uk-toggle="target: #comeca-ja" type="button" class="uk-button uk-button-primary round">Começa ja!</button>
                    </div>

                    <div class="col s12 m6 l6"><br><br><br><br><br>
                        <img width="350" height="350" src="../static/imagens/imagem3.png">
                    </div>
                </div>
                <br><br><br><br><br><br>

            </section><br><br><br><br><br>

            <!-- Footer -->
            <footer>

                <div class="container row">

                    <div class="col s12 m6 l6"><br>
                        <h5 class="h5footer">SOBRE MYSCHEDULE</h5><br>
                        <h6>Contacta-nos!</h6>
                        <h6>Termos de Serviço e de Privacidade</h6>
                        <a href="https://undraw.co/">Imagens: Undraw</a><br>
                        <br>
                    </div>

                    <div class="col s12 m6 l6">
                        <img class="imagemfooter" width="200" height="200" src="../static/imagens/imagem4.png">
                    </div>

                </div>

            </footer>
        </body>

        </html>