    <!-- MySchedule - Carlos Ferreira -->
    <?php session_start(); ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>myschedule</title>
      <link rel="icon" href="../static/imagens/logo_2.png" type="image/ico">

      <link rel="stylesheet" href="../static/css/index.css">
      <link rel="stylesheet" href="../static/css/uikit/uikit.css">
      <link rel="stylesheet" href="../static/css/materialize/css/materialize.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

      <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
      <script src="../static/js/app.js"></script>
      <script src="../static/js/uikit/uikit.min.js"></script>
      <script src="../static/js/uikit/uikit-icons.min.js"></script>
      <style>
        header,
        main,
        footer {
          padding-right: 230px;
        }

        a.float {
          right: 270px !important;
        }


        @media only screen and (max-width : 992px) {

          header,
          main,
          footer {
            padding-right: 0;
          }

          a.float {
            right: 0px !important;
          }

        }
      </style>
    </head>

    <body class="white">
      <header>
        <ul id="dropdown1" class="dropdown-content">
          <li><a href="../html/todo.php?order=1">Importância</a></li>
          <li><a href="../html/todo.php?order=2">Data limite</a></li>
          <li><a href="../html/todo.php?order=3">Nome</a></li>
        </ul>
        <div class="navbar">
          <nav class="white" style="padding: 0 0 75px 0">
            <div class="nav-wrapper">
              <ul class="container-fluid">
                <li class="left"><a href="#" data-target="slide-out" class="black-text show-on-medium-and-up sidenav-trigger left"><i class="material-icons">menu</i></a></li>
                <li class="left"><img width="47" height="47" src="../static/imagens/logo_2.png"></li>
                <li class="left"><a class="logo blue-text" href="http://localhost/myschedule/html/todo.php">myschedule</a></li>
                <li class="right"><a class="dropdown-trigger" href="#!" data-target="dropdown1">Ordem<i class="material-icons right">arrow_drop_down</i></a></li>
              </ul>
            </div>
          </nav>
        </div>

        <ul id="slide-out" class="sidenav"><br>
          <li><a href="http://localhost/myschedule/html/actions.php"><i class="material-icons">note</i>Notas</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a href="http://localhost/myschedule/html/todo.php"><i class="material-icons">schedule</i>Tarefas</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a onClick="openMyAccount();"><i class="material-icons">person</i>A Minha Conta</a></li>

          <form action="../server/classes/index/logout.php" method="POST">
            <input type="submit" class="uk-button uk-button-primary round" style="margin: 65px; margin-bottom: -450px;" name="log-out" value="Sign Out">
          </form>
        </ul>

        <ul id="sidenav_right_side" class="sidenav sidenav-fixed" style="width:230px !important;"><br>
          <li>
            <h6 class="center"><i class="material-icons">more_horiz</i></h6>
          </li>
          <li><a href="http://localhost/MySchedule/html/todo.php"><i class="material-icons">schedule</i>Tarefas</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a href="http://localhost/MySchedule/html/bin.php"><i class="material-icons">delete</i>Lixo</a></li>
          <li>
            <div class="divider"></div>
          </li>
          <li><a href="http://localhost/MySchedule/html/notificacoes.php"><i class="material-icons">sms</i>Notificações</a></li>
          <li>
            <div class="divider"></div>
          </li>
        </ul>
      </header>

      <main>
        <?php
        if (isset($_GET['modal'])) {
          $modal = $_GET['modal'];
          if ($modal == "changedprofile") {
            echo ("<script>UIkit.modal($('#profileupdated')).show();</script>");
          }
        }

        if (isset($_GET['error'])) {
          echo ("<script>M.toast({html: 'Campos inválidos', classes: 'rounded'});</script>");
        }
        ?>

        <?php
        require '../server/classes/database.php';
        $session_id = $_SESSION['userid'];
        //Receber todos os dados do utilizador
        $sql_get_data_user = "SELECT * FROM users WHERE idUtilizador = ?";
        $stmt_get_data_user = mysqli_stmt_init($connection);

        if (!mysqli_stmt_prepare($stmt_get_data_user, $sql_get_data_user)) {
          header("Location: ../../todo.php");
          exit();
        } else {
          mysqli_stmt_bind_param($stmt_get_data_user, "s", $session_id);
          mysqli_stmt_execute($stmt_get_data_user);

          $result = mysqli_stmt_get_result($stmt_get_data_user);

          if ($row_get_data_user = mysqli_fetch_assoc($result)) {
            $username = $row_get_data_user['nomeUtilizador'];
            $email = $row_get_data_user['emailUtilizador'];
            ?>
            <input type="hidden" name="nomestorage" id="nomestorage" value="<?php echo $username; ?>">
            <input type="hidden" name="emailstorage" id="emailstorage" value="<?php echo $email; ?>">

          <?php
        } else {
          echo "<script>console.log('asd');</script>";
        }
      }
      ?>

        <!-- Modal - A minha conta -->
        <div id="aminhaconta" uk-modal>
          <div class="uk-modal-dialog uk-modal-body" style="width: 850px !important;">
            <h4 class="center" style="font-weight: 100 !important; font-size: 25px !important;">A Minha Conta</h4>
            <div class="row">
              <div class="col s12 m6">
                <div class="row">
                  <form class="col s12" method="POST" action="./../server/classes/profile/changeprofile.php">
                    <br>
                    <div class="input-field col s9">
                      <i class="material-icons prefix">account_circle</i>
                      <input id="nome" type="text" class="validate" name="nome">
                    </div>
                </div>
                <div class="row">
                  <div class="input-field col s9">
                    <i class="material-icons prefix">email</i>
                    <input id="icon_telephone" type="text" class="validate" name="icon_telephone">
                  </div>
                </div>
                <input style="margin-left: 298px !important;" type="submit" class="uk-button uk-button-default" name="changeprofile" value="Guardar">
                </form>
              </div>
            </div><br>
            <div class="col s12 m6">
              <img src="./../static/imagens/profilepic.png" style="width: 270px !important;">
            </div>
          </div>
        </div>
        </div>
        <!-- Mostrar Tarefas --><br><br>
        <div class="container">
          <div class="timeline">
            <?php
            require '../server/classes/database.php';

            $iduser = $_SESSION['userid'];
            $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 1";

            if (isset($_GET['order'])) {
              $value = $_GET['order'];
              // 1 - Importância
              // 2 - Data Limite
              // 3 - Nome
              if ($value == 1) {
                $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 1 ORDER BY importanciaTask DESC";
              } else if ($value == 2) {
                $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 1 ORDER BY dataLimiteTask";
              } else {
                $sql_select_all_tasks = "SELECT * FROM tasks WHERE idUtilizador = ? AND status = 1 ORDER BY nomeTask";
              }
            }

            $stmt_select_all_tasks = mysqli_stmt_init($connection);

            if (!mysqli_stmt_prepare($stmt_select_all_tasks, $sql_select_all_tasks)) {
              exit();
            } else {
              mysqli_stmt_bind_param($stmt_select_all_tasks, "s", $iduser);
              mysqli_stmt_execute($stmt_select_all_tasks);

              $result_query_select_all_tasks = mysqli_stmt_get_result($stmt_select_all_tasks);

              $cor = "azul";
              while ($row = $result_query_select_all_tasks->fetch_array()) {
                $nome = $row['nomeTask'];
                $prioridade = $row['importanciaTask'];
                $datalimite = $row['dataLimiteTask'];
                $id_task = $row['idTask'];
                $status = $row['status'];

                if ($prioridade == 1) {
                  $cor = "blue";
                } else if ($prioridade == 2) {
                  $cor = "yellow";
                } else {
                  $cor = "red";
                }

                $cor_completo = "timeline-badge white-text " . $cor;
                ?>

                <div class="timeline-event">
                  <div class="card timeline-content">
                    <form action="" method="POST">
                      <div class="card-content" valormodal="valormodal" id="<?php echo $id_task ?>">
                        <span id="id_task" style="display: none;"><?php echo $id_task; ?></span>
                        <p id="name_task" style="display:none"><?php echo $nome; ?></p>
                        <p id="priority_task_to_write" style="display:none"><?php echo $prioridade ?></p>
                        <input id="datetime_write_task" type="hidden" value="<?php echo $datalimite; ?>">

                        <div class="uk-inline right">
                          <i class='material-icons right blue-text' style='line-height: 23px !important;'>done</i>
                          <i class="material-icons right" style="line-height: 23px !important;">more_vert</i>
                          <div uk-dropdown="pos: right-left; duration: 100">
                            <ul class="uk-nav uk-dropdown-nav">
                              <li> <a href="../server/classes/todo/delete.php?id=<?php echo ($id_task); ?>"><i class="material-icons" style="vertical-align: sub !important;">delete_outline</i> Eliminar</a></li>
                              <li> <a href="../server/classes/todo/recuperarnota.php?id=<?php echo $id_task; ?>"> <i class="material-icons" style="vertical-align: sub !important;">cached</i> Recuperar tarefa</a></li>
                          </div>
                        </div>
                        <?php echo "<font size=5>" . $nome . "</font><br><font size=2>(" . $datalimite . ")</font>" ?>
                      </div>
                    </form>
                  </div>
                </div>
              <?php
            }
          }
          ?>
          </div>
        </div>
      </main>
      <script src="../static/js/app.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.js"></script>

    </body>

    </html>